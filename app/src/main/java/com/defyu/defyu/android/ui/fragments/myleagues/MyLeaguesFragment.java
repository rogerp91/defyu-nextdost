package com.defyu.defyu.android.ui.fragments.myleagues;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;

import com.defyu.defyu.android.data.models.MyLeagues;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.adapters.MyLeaguesAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.ui.fragments.menu.NotificationsFragment;
import com.defyu.defyu.android.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyLeaguesFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = MyLeaguesFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;
    private MyLeaguesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<MyLeagues> myLeagues;

    @BindView(R.id.tvJoinLeague)
    TextView tvJoinLeague;
    @BindView(R.id.tvCreateOne)
    TextView tvCreateOne;
    @BindView(R.id.fmFirstTime)
    FrameLayout fmFirstTime;
    @BindView(R.id.svContent)
    ScrollView svContent;

    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.list_public)
    RecyclerView list_public;
    @BindView(R.id.list_private)
    RecyclerView list_private;

    public MyLeaguesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MyLeaguesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyLeaguesFragment newInstance() {
        return new MyLeaguesFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_my_leagues;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.my_leagues);

        if(Session.isFirstTimeMyLeague()){
            fmFirstTime.setVisibility(View.VISIBLE );
            svContent.setVisibility(View.GONE);
        }else {
            fmFirstTime.setVisibility(View.GONE );
            svContent.setVisibility(View.VISIBLE);
            recyclerDefyu();
            recyclerPublic();
            recyclerPrivate();
        }


        tvCreateOne.setOnClickListener(this);
        tvJoinLeague.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCreateOne:
                //simpleToast("Create one");
                Session.setKeyFirstTimeMyLeagues(false);
                onViewReady(null,null,null,null);
                break;
            case R.id.tvJoinLeague:
                simpleToast("join League");
                break;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_notification, menu);
        menu.removeItem(R.id.menu_notification);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_notification_on:
                pushFragment(new NotificationsFragment());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void recyclerDefyu(){

        initializeDataDefyu();

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);
        list.setLayoutManager(mLayoutManager);

        mAdapter = new MyLeaguesAdapter(myLeagues);

        list.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                simpleToast("Salary Cap DefyU");

            }
        }));

        list.setAdapter(mAdapter);

    }

    private void recyclerPublic(){
        initializeDataPublic();

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);
        list_public.setLayoutManager(mLayoutManager);

        mAdapter = new MyLeaguesAdapter(myLeagues);

        list_public.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                simpleToast("Salary Cap Public");

            }
        }));

        list_public.setAdapter(mAdapter);
    }

    private void recyclerPrivate(){
        initializeDataPrivate();

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);
        list_private.setLayoutManager(mLayoutManager);

        mAdapter = new MyLeaguesAdapter(myLeagues);

        list_private.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                simpleToast("Salary Cap Private");

            }
        }));

        list_private.setAdapter(mAdapter);
    }

    private void initializeDataDefyu(){
        myLeagues = new ArrayList<>();
        myLeagues.add(new MyLeagues("130", "BARCELONA", "124M", "9", "13", "DEADLINE: 1 DAYS"));
        myLeagues.add(new MyLeagues("150", "BARCELONA", "170M", "9", "13", "DEADLINE: 2 DAYS 1 HOUR"));
        myLeagues.add(new MyLeagues("165", "BARCELONA", "155M", "8", "17", "DEADLINE: 5 DAYS 12 HOUR"));
        myLeagues.add(new MyLeagues("100", "BARCELONA", "190M", "9", "25", "DEADLINE: 3 DAYS 10 HOUR"));
        myLeagues.add(new MyLeagues("132", "BARCELONA", "220M", "10", "1", "DEADLINE: 2 DAYS 3 HOUR"));
        myLeagues.add(new MyLeagues("157", "BARCELONA", "170M", "9", "12", "DEADLINE: 3 WEEKS 1 DAY"));
        myLeagues.add(new MyLeagues("190", "BARCELONA", "167M", "9", "10", "DEADLINE: 2 HOUR 30 MIN"));
        myLeagues.add(new MyLeagues("200", "BARCELONA", "120M", "8", "7", "DEADLINE: 2 WEEKS"));
        myLeagues.add(new MyLeagues("330", "BARCELONA", "160M", "9", "2", "DEADLINE: 2 DAYS 1 HOUR"));
    }

    private void initializeDataPublic() {
        myLeagues = new ArrayList<>();
        myLeagues.add(new MyLeagues("130", "PRO LEAGUE", "124M", "9", "13", "DEADLINE: 1 DAYS"));
        myLeagues.add(new MyLeagues("150", "PRO LEAGUE", "170M", "9", "13", "DEADLINE: 2 DAYS 1 HOUR"));
        myLeagues.add(new MyLeagues("165", "PRO LEAGUE", "155M", "8", "17", "DEADLINE: 5 DAYS 12 HOUR"));
        myLeagues.add(new MyLeagues("100", "PRO LEAGUE", "190M", "9", "25", "DEADLINE: 3 DAYS 10 HOUR"));
        myLeagues.add(new MyLeagues("132", "PRO LEAGUE", "220M", "10", "1", "DEADLINE: 2 DAYS 3 HOUR"));
        myLeagues.add(new MyLeagues("157", "PRO LEAGUE", "170M", "9", "12", "DEADLINE: 3 WEEKS 1 DAY"));
        myLeagues.add(new MyLeagues("190", "PRO LEAGUE", "167M", "9", "10", "DEADLINE: 2 HOUR 30 MIN"));
        myLeagues.add(new MyLeagues("200", "PRO LEAGUE", "120M", "8", "7", "DEADLINE: 2 WEEKS"));
        myLeagues.add(new MyLeagues("330", "PRO LEAGUE", "160M", "9", "2", "DEADLINE: 2 DAYS 1 HOUR"));
    }

    private void initializeDataPrivate(){
        myLeagues = new ArrayList<>();
        myLeagues.add(new MyLeagues("130", "LEAGUE 2", "124M", "9", "13", "DEADLINE: 1 DAYS"));
        myLeagues.add(new MyLeagues("150", "LEAGUE 2", "170M", "9", "13", "DEADLINE: 2 DAYS 1 HOUR"));
        myLeagues.add(new MyLeagues("165", "LEAGUE 2", "155M", "8", "17", "DEADLINE: 5 DAYS 12 HOUR"));
        myLeagues.add(new MyLeagues("100", "LEAGUE 2", "190M", "9", "25", "DEADLINE: 3 DAYS 10 HOUR"));
        myLeagues.add(new MyLeagues("132", "LEAGUE 2", "220M", "10", "1", "DEADLINE: 2 DAYS 3 HOUR"));
        myLeagues.add(new MyLeagues("157", "LEAGUE 2", "170M", "9", "12", "DEADLINE: 3 WEEKS 1 DAY"));
        myLeagues.add(new MyLeagues("190", "LEAGUE 2", "167M", "9", "10", "DEADLINE: 2 HOUR 30 MIN"));
        myLeagues.add(new MyLeagues("200", "LEAGUE 2", "120M", "8", "7", "DEADLINE: 2 WEEKS"));
        myLeagues.add(new MyLeagues("330", "LEAGUE 2", "160M", "9", "2", "DEADLINE: 2 DAYS 1 HOUR"));
    }


}
