package com.defyu.defyu.android.ui.fragments.myleagues;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.adapters.FooterAdapter;
import com.defyu.defyu.android.ui.customersview.FooterView;
import com.defyu.defyu.android.ui.customersview.MyPagerView;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.ui.fragments.menu.EarnCoinsFragment;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link MainPagerLeaguesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainPagerLeaguesFragment extends BaseFragment implements FooterView.OnTabChange, View.OnClickListener {
    private static final String TAG = MainPagerLeaguesFragment.class.getSimpleName();
    public static int EXTRA_TICKETS_DEFAULT = 3;
    public static int VIRTUAL_COINS_DEFAULT = 1000;

    private FooterAdapter mPagerAdapter;

    //HEADER
    @BindView(R.id.extra)
    TextView extra_ticket;
    @BindView(R.id.coins)
    TextView virtual_coins;
    @BindView(R.id.shop)
    LinearLayout shop;
    @BindView(R.id.earn_coins)
    LinearLayout earn_coins;
    //------

    @BindView(R.id.pager)
    MyPagerView pager;
    @BindView(R.id.footer)
    FooterView footerView;

    public MainPagerLeaguesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainPagerLeaguesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainPagerLeaguesFragment newInstance() {
        return new MainPagerLeaguesFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_main_pager_leagues;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        mPagerAdapter = new FooterAdapter(getChildFragmentManager());
        pager.setAdapter(mPagerAdapter);
        footerView.setupWithViewPager(pager);

        extra_ticket.setText(String.valueOf(EXTRA_TICKETS_DEFAULT));
        virtual_coins.setText(String.valueOf(VIRTUAL_COINS_DEFAULT) + "K");

        shop.setOnClickListener(this);
        earn_coins.setOnClickListener(this);
        footerView.setOnTabChange(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onTabSelected(int position) {
        switch (position) {
            case 1:
                simpleToast("this feature is not available yet");
                break;
            case 2:
                simpleToast("this feature is not available yet");
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shop:
                simpleToast(R.string.shop);
                break;
            case R.id.earn_coins:
                pushFragment(new EarnCoinsFragment());
                break;
        }
    }
}
