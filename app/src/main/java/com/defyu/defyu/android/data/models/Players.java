package com.defyu.defyu.android.data.models;

import java.util.List;

/**
 * Created by Rp on 23/12/2016.
 */

public class Players {

    private String name;
    private String avatar;
    private List<League> leagues;

    public Players(String name, String avatar, List<League> leagues) {
        this.name = name;
        this.avatar = avatar;
        this.leagues = leagues;
    }

    public Players(String name, String avatar) {
        this.name = name;
        this.avatar = avatar;
    }

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}