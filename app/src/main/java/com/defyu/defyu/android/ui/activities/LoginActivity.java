package com.defyu.defyu.android.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.ui.fragments.login.LoginButtonsFragment;
import com.defyu.defyu.android.ui.fragments.login.LoginFragment;
import com.defyu.defyu.android.ui.fragments.login.OnboardingFragment;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class LoginActivity extends BaseActivity implements LoginActivityInterface {
    private static final String TAG = LoginActivity.class.getSimpleName();


    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            if(Session.isFirstTime()){
                pushFragment(OnboardingFragment.newInstance(),R.id.container, false);
            }else {
                pushFragment(LoginButtonsFragment.newInstance(), R.id.container, false, TRANSACTION_WITHOUT_ANIMATION);
            }
        }
    }
}
