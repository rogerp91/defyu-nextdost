package com.defyu.defyu.android.ui.customersview;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;

import org.w3c.dom.Text;

/**
 * Created by dars_ on 14/12/2016.
 */

public class EditUsername extends LinearLayout implements TextWatcher {

    private EditText etUsername;
    private ImageView ivCheck;
    private LinearLayout llSugerence;
    private TextView tvSugerence;
    private TextWatcher textWatcher = null;

    public EditUsername(Context context) {
        super(context);
        init();
    }

    public EditUsername(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditUsername(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        //Utilizamos el layout 'control_login' como interfaz del control
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li =
                (LayoutInflater)getContext().getSystemService(infService);
        li.inflate(R.layout.view_edit_username, this, true);

        //Obtenemoslas referencias a los distintos control
        etUsername = (EditText)findViewById(R.id.etUsername);
        ivCheck = (ImageView) findViewById(R.id.ivCheck);
        llSugerence = (LinearLayout) findViewById(R.id.llSugerence);
        tvSugerence = (TextView) findViewById(R.id.tvSugerence);

        //Asociamos los eventos necesarios
        etUsername.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(textWatcher != null){
            textWatcher.beforeTextChanged(charSequence, i,i1,i2);
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(textWatcher != null){
            textWatcher.onTextChanged(charSequence, i,i1,i2);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(textWatcher != null){
            textWatcher.afterTextChanged(editable);
        }
    }

    public void addTextChangedListener( TextWatcher textWatcher){
        if(textWatcher != null){
            this.textWatcher = textWatcher;
        }
    }

    public void removeTextChangedListener(){
        this.textWatcher = null;
    }


    public boolean isEmpty(){
        return etUsername.getText().toString().isEmpty();
    }

    public String getText(){
        return etUsername.getText().toString();
    }

    public void setCheckStatus(boolean good){
        if(good){
            ivCheck.setImageResource(R.drawable.ic_check_green);
        }else{
            ivCheck.setImageResource(R.drawable.ic_check);
        }
    }

    public void setOnError(boolean error){
        if(error){
            etUsername.getBackground().setColorFilter(getResources().getColor(R.color.red_error), PorterDuff.Mode.SRC_ATOP);
            ivCheck.setImageResource(R.drawable.ic_check);
        }else{
            etUsername.getBackground().setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public void setSugerence(String sugerence){
        if(sugerence != null){
            llSugerence.setVisibility(View.VISIBLE);
            tvSugerence.setText(sugerence);
        }else{
            llSugerence.setVisibility(View.INVISIBLE);
        }
    }

    public void setText(String text) {
        this.etUsername.setText(text);
    }
}
