package com.defyu.defyu.android.ui.fragments.menu;


import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link AboutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutFragment extends BaseFragment {
    private static final String TAG = AboutFragment.class.getSimpleName();

    @BindView(R.id.tvVersionName)
    TextView tvVersionName;


    public AboutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AboutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_about);
        setEnableBackToolbar(true);
        try {
            String app_ver = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            tvVersionName.setText(getString(R.string.version_name,app_ver));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG+" Dars",e.getLocalizedMessage());
        }
    }


}
