package com.defyu.defyu.android.ui.fragments.myleagues;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

public class JoinLeagueFragment extends BaseFragment implements View.OnClickListener{
    private static final String TAG = JoinLeagueFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;

    @BindView(R.id.game_rules)
    TextView game_rules;
    @BindView(R.id.join_league)
    Button join_league;

    public JoinLeagueFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment JoinLeagueFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JoinLeagueFragment newInstance() {
        return new JoinLeagueFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_join_league;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.join_a_league);
        setEnableBackToolbar(true);

        game_rules.setOnClickListener(this);
        join_league.setOnClickListener(this);

    }

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.game_rules:
                simpleToast("This feature is not available yet");
                break;
            case R.id.join_league:

                break;
        }
    }
}



