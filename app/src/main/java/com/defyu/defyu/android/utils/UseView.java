package com.defyu.defyu.android.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by AF on 23/12/2016.
 */

public class UseView {

    /**
     * Mostrar o no mostar una vista
     *
     * @param active  {@link Boolean}
     * @param view    {@link View}
     * @param context {@link Context}
     * @param isAdded {@link Fragment#isAdded()}
     */
    public static void showOrHideView(final boolean active, final View view, Context context, boolean isAdded) {
        if (!isAdded) {// onPause o onDestroy
            return;
        }
        int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);
        view.setVisibility(active ? View.VISIBLE : View.GONE);
        view.animate().setDuration(shortAnimTime).alpha(active ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(active ? View.VISIBLE : View.GONE);
            }
        });
    }

}