package com.defyu.defyu.android.ui.fragments.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.ui.customersview.EditUsername;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginActivityInterface} interface
 * to handle interaction events.
 * Use the {@link CreateUsernameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateUsernameFragment extends BaseFragment implements TextWatcher, View.OnClickListener {
    private static final String TAG = CreateUsernameFragment.class.getSimpleName();
    private static final String KEY_FULL_NAME = "full_name";

    private LoginActivityInterface mListener;
    private String fullName;
    private String[] busies = {"jhondoe","dorianalexiis","jolivieri","igary","tundisi","mariexi","jeastman"};

    @BindView(R.id.euUsername)
    EditUsername euUsername;
    @BindView(R.id.btnStart)
    Button btnStart;

    public CreateUsernameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CreateUsernameFragment.
     * @param full_name
     */
    // TODO: Rename and change types and number of parameters
    public static CreateUsernameFragment newInstance(String full_name) {
        CreateUsernameFragment createUsernameFragment = new CreateUsernameFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_FULL_NAME, full_name);
        createUsernameFragment.setArguments(bundle);
        return createUsernameFragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            fullName = getArguments().getString(KEY_FULL_NAME);
            fullName = fullName.replace(" ", "").toLowerCase();
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_create_username;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        euUsername.setText(fullName);
        if(fullName.length()> 4){
            if(isValid()){
                euUsername.setOnError(false);
                euUsername.setSugerence(null);
                euUsername.setCheckStatus(true);
            }else{
                euUsername.setOnError(true);
                euUsername.setCheckStatus(false);
                euUsername.setSugerence(euUsername.getText()+"555");
            }
        }
        euUsername.addTextChangedListener(this);
        btnStart.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean isValid() {
        String username = euUsername.getText();
        boolean valid = true;
        if (username.length() <5){
            return false;
        }

        for (String busy : busies){
            if(busy.equals(username)){
                valid = false;
            }
        }

        return valid;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        btnStart.setBackground(getResources().getDrawable(R.drawable.btn_green_transparent));
        btnStart.setTextColor(getResources().getColor(R.color.whiteTransparent));
        euUsername.setCheckStatus(false);
        if(editable.length() < 5){
            return;
        }

        if(!isValid()){
            euUsername.setOnError(true);
            euUsername.setSugerence(euUsername.getText()+"555");
            return;
        }else {
            euUsername.setOnError(false);
            euUsername.setSugerence(null);
        }
        euUsername.setCheckStatus(true);
        btnStart.setBackgroundResource(R.drawable.btn_green);
        btnStart.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnStart:
                if(!isValid()){
                    return;
                }
                simpleToast("Start");
            break;
        }
    }
}
