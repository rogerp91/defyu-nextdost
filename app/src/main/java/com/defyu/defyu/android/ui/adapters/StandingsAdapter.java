package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Standings;

import java.util.List;

public class StandingsAdapter extends RecyclerView.Adapter<StandingsAdapter.StandingsViewHolder> {
    List<Standings> list;
    Context context;

    public static class StandingsViewHolder extends RecyclerView.ViewHolder {
        TextView position;
        ImageView photo;
        TextView full_name;
        TextView points;
        View vwLast;

        StandingsViewHolder(View itemView) {
            super(itemView);
            position = (TextView)itemView.findViewById(R.id.position);
            photo = (ImageView) itemView.findViewById(R.id.photo);
            full_name = (TextView)itemView.findViewById(R.id.full_name);
            points = (TextView)itemView.findViewById(R.id.points);
            vwLast = itemView.findViewById(R.id.vwLast);
        }
    }

    public StandingsAdapter(List<Standings> list){
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public StandingsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_standings, viewGroup, false);
        StandingsViewHolder lvh = new StandingsViewHolder(v);
        context = viewGroup.getContext();
        return lvh;
    }


    @Override
    public void onBindViewHolder(final StandingsViewHolder holder, int i) {
        Glide.with(context)
                .load(list.get(i).getPhoto())
                .into(holder.photo);
        holder.position.setText(String.valueOf(list.get(i).getPosition()));
        holder.full_name.setText(list.get(i).getFull_name());
        holder.points.setText(String.valueOf(list.get(i).getPoints()));

        holder.vwLast.setVisibility((i == (list.size()-1))? View.VISIBLE: View.GONE);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Standings getItemPosition(int position){
        return list.get(position);
    }

}
