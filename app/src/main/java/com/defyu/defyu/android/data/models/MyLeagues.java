package com.defyu.defyu.android.data.models;


import java.util.ArrayList;
import java.util.List;

public class MyLeagues {
    private String coins;
    private String league;
    private String price;
    private String players;
    private String rating;
    private String deadline;

    public MyLeagues(String coins, String league, String price, String players, String rating, String deadline) {
        this.coins = coins;
        this.league = league;
        this.price = price;
        this.players = players;
        this.rating = rating;
        this.deadline = deadline;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getCoins() {
        return coins;
    }

    public void setCoins(String coins) {
        this.coins = coins;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

}

