package com.defyu.defyu.android.data.models;

/**
 * Created by dars_ on 22/12/2016.
 */

public class Trophy {
    private String league;
    private int coints;

    public Trophy(String league, int coints) {
        this.league = league;
        this.coints = coints;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public int getCoints() {
        return coints;
    }

    public void setCoints(int coints) {
        this.coints = coints;
    }
}
