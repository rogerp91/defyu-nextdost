package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;


import butterknife.BindView;

public class AddressFragment extends BaseFragment {
    private static final String TAG = AddressFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;

    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.street)
    TextView street;
    @BindView(R.id.state)
    TextView state;
    @BindView(R.id.zip)
    TextView zip;

    public AddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddressFragment newInstance() {
        return new AddressFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_address;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.address);
        setEnableBackToolbar(true);

        country.setText("Spain");
        city.setText("Barcelona");
        street.setText("Street 45 - San Luis");
        state.setText("Barcelona");
        zip.setText("85014");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
        setHasOptionsMenu(true);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save:
                simpleToast(R.string.save_address);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

