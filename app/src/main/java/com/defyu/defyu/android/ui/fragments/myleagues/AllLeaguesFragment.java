package com.defyu.defyu.android.ui.fragments.myleagues;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.League;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.adapters.AllLeaguesAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.ui.viewholders.AllLeaguesViewholder;

import butterknife.BindView;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com).
 */

public class AllLeaguesFragment extends BaseFragment {

    // ---------------------------------------------------------------------------------------------
    // Constansts
    // ---------------------------------------------------------------------------------------------

    private static final String TAG = AllLeaguesFragment.class.getSimpleName();

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    @BindView(R.id.rv_defyu)
    RecyclerView rvDefyu;
    @BindView(R.id.rv_public)
    RecyclerView rvPublic;
    @BindView(R.id.rv_pending_invites)
    RecyclerView rvPendingInvites;

    private AllLeaguesAdapter mDefyuAdapter;
    private AllLeaguesAdapter mPublicAdapter;
    private AllLeaguesAdapter mPendingInvitesAdapter;
    private MainActivityInterface mListener;

    // ---------------------------------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------------------------------

    public AllLeaguesFragment() {
        // Required empty public constructor
    }

    public static AllLeaguesFragment newInstance() {
        return new AllLeaguesFragment();
    }

    // ---------------------------------------------------------------------------------------------
    // Lifecycle
    // ---------------------------------------------------------------------------------------------

    @Override
    public int getLayoutView() {
        return R.layout.fragment_all_leagues;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHaveToolbar(true);
        //setHasOptionsMenu(true);
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.all_leagues_title);
        //setEnableBackToolbar(false);

        initDefyuItems();
        initPublicItems();
        initPendingInvitesItems();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityInterface) {
            mListener = (MainActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MainActivityInterface");
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Methods
    // ---------------------------------------------------------------------------------------------

    private void initDefyuItems() {
        if (mDefyuAdapter == null) {
            mDefyuAdapter = new AllLeaguesAdapter(getContext());
            mDefyuAdapter.setOnClickListener(getOnClickListener());
        }

        rvDefyu.setHasFixedSize(true);
        rvDefyu.setLayoutManager(getLinearLayoutManager());
        rvDefyu.setAdapter(mDefyuAdapter);
    }

    private void initPublicItems() {
        if (mPublicAdapter == null) {
            mPublicAdapter = new AllLeaguesAdapter(getContext());
            mPublicAdapter.setOnClickListener(getOnClickListener());
        }

        rvPublic.setHasFixedSize(true);
        rvPublic.setLayoutManager(getLinearLayoutManager());
        rvPublic.setAdapter(mPublicAdapter);
    }

    private void initPendingInvitesItems() {
        if (mPendingInvitesAdapter == null) {
            mPendingInvitesAdapter = new AllLeaguesAdapter(getContext());
            mPendingInvitesAdapter.setOnClickListener(getOnClickListener());
        }

        rvPendingInvites.setHasFixedSize(true);
        rvPendingInvites.setLayoutManager(getLinearLayoutManager());
        rvPendingInvites.setAdapter(mPendingInvitesAdapter);
    }

    private AllLeaguesViewholder.OnClickListener getOnClickListener() {
        return new AllLeaguesViewholder.OnClickListener() {
            @Override
            public void onJoin(League league) {

            }
        };
    }

    private LinearLayoutManager getLinearLayoutManager() {
        return new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
    }
}
