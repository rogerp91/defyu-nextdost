package com.defyu.defyu.android.ui.fragments.search;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.League;
import com.defyu.defyu.android.data.models.Players;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.customersview.CircularImageView;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.UseView;
import com.github.roger91.mlprogress.MlProgress;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Rp on 22/12/2016.
 */

public class SearchFragment extends BaseFragment implements
        AdapterView.OnItemClickListener,
        View.OnKeyListener {

    private static final String TAG = SearchFragment.class.getSimpleName();

    //Instances
    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------
    /**
     * @see MainActivityInterface
     */
    private MainActivityInterface mListener;

    @BindView(R.id.search)
    AutoCompleteTextView mSearch;

    @BindView(R.id.progress)
    MlProgress mProgress;

//    @BindView(R.id.search_text)
//    TextView mSearchText;

    @BindView(R.id.no_result)
    TextView mNoResult;

    @BindView(R.id.container_players_leagues)
    LinearLayout mContainerPlayersLeagues;

    @BindView(R.id.container_progress_text)
    FrameLayout mContainerProgressText;

    CircularImageView mAvatar;
    @BindView(R.id.title)
    TextView mTitle;

    // Ejemplos
    private List<String> names;
    private List<Players> playerses;
    private ArrayAdapter<String> adapter;

    int pos = -1;
    Handler handler = null;
    List<Players> aux;

    @BindView(R.id.lengues1)
    TextView mLengues1;
    @BindView(R.id.lengues2)
    TextView mLengues2;
    @BindView(R.id.lengues3)
    TextView mLengues3;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_search;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);

        //Load data
        names = new ArrayList<>();
        names.add("Javier Mascherano");
        names.add("Lionel Messi");
        names.add("David Villa");
        names.add("Philipp Lahm");

        playerses = new ArrayList<>();

        //1 Jugador de ejemplo
        List<League> leagues1 = new ArrayList<>();
        leagues1.add(new League("Copa América 2004"));
        leagues1.add(new League("Copa América 2007"));
        leagues1.add(new League("Copa América 2011"));
        playerses.add(new Players("Javier Mascherano", "http://cdn.sofifa.org/17/players/142754.png", leagues1));

        //2 Jugador de ejemplo
        List<League> leagues2 = new ArrayList<>();
        leagues2.add(new League("Copa Mundial de Clubes 2015"));
        leagues2.add(new League("Liga de España 2010"));
        leagues2.add(new League("Trofeo Joan Gamper 2016"));
        playerses.add(new Players("Lionel Messi", "http://cdn.sofifa.org/17/players/158023.png", leagues2));

        //3 Jugador de ejemplo
        List<League> leagues3 = new ArrayList<>();
        leagues3.add(new League("Eurocopa 2008"));
        leagues3.add(new League("Copa Confederaciones 2009"));
        leagues3.add(new League("Mundial de Fútbol de 2014"));
        playerses.add(new Players("David Villa", "http://cdn.sofifa.org/17/players/113422.png", leagues3));

        //4 Jugador de ejemplo
        List<League> leagues4 = new ArrayList<>();
        leagues4.add(new League("Eurocopa 2012"));
        leagues4.add(new League("Eurocopa 2008"));
        leagues4.add(new League("Mundial de Fútbol de 2014"));
        playerses.add(new Players("Philipp Lahm", "http://cdn.sofifa.org/17/players/121939.png", leagues4));

    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setEnableBackToolbar(true);
        setTitle(getString(R.string.title_search));
        mAvatar = (CircularImageView) view.findViewById(R.id.avatar);
        showOrHideView(Boolean.TRUE, mContainerProgressText);//Show Container Layout
        showOrHideView(Boolean.TRUE, mNoResult);//Show text
        mSearch.setOnKeyListener(this);
        mSearch.setOnItemClickListener(this);
        adapter = new ArrayAdapter<>(getActivity(), R.layout.li_query_suggestion, names);
        mSearch.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityInterface) {
            mListener = (MainActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        searchPlayer((String) adapterView.getItemAtPosition(i));
    }

    private void searchPlayer(String name) {
        mSearch.setEnabled(Boolean.FALSE);
        showOrHideView(Boolean.FALSE, mNoResult);//Hide text
        showOrHideView(Boolean.TRUE, mProgress);// Show Progress
        for (String string : names)
            if (string.toLowerCase().equals(name.toLowerCase())) {
                pos = names.indexOf(string);
            }


        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showOrHideView(Boolean.TRUE, mContainerPlayersLeagues);
                aux = new ArrayList<>(playerses);
                Glide.with(getActivity()).load(aux.get(pos).getAvatar()).asBitmap().into(new SimpleTarget<Bitmap>(100, 100) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        mAvatar.setImageBitmap(resource);
                    }
                });

                mTitle.setText(aux.get(pos).getName());
                mLengues1.setText(aux.get(pos).getLeagues().get(0).getLeauge());
                mLengues2.setText(aux.get(pos).getLeagues().get(1).getLeauge());
                mLengues3.setText(aux.get(pos).getLeagues().get(2).getLeauge());
                //View
                mSearch.setEnabled(Boolean.TRUE);
                showOrHideView(Boolean.FALSE, mProgress);
            }
        }, 1000);

    }

    public void showOrHideView(boolean active, final View view) {
        UseView.showOrHideView(active, view, getActivity().getApplicationContext(), isAdded());
    }

    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == KeyEvent.KEYCODE_DEL) {// Tecla borrar, para recargar de nuevo
            handler = null;
            showOrHideView(Boolean.FALSE, mNoResult);//Show result
            showOrHideView(Boolean.TRUE, mNoResult);//Show text
            pos = -1;
            showOrHideView(Boolean.FALSE, mProgress);
            showOrHideView(Boolean.FALSE, mContainerPlayersLeagues);
        }
        return false;
    }


}