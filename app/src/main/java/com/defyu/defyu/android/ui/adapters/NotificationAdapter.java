package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.NotificationInApp;
import com.defyu.defyu.android.ui.viewholders.NotificationViewholder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com) on 17-12-2016.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewholder> {

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    private Context mContext;
    private List<NotificationInApp> mList;
    private NotificationViewholder.OnClickListener mListener;

    // ---------------------------------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------------------------------

    public NotificationAdapter(Context context) {
        mContext = context;
        mList = new ArrayList<>();
        test();
    }

    // ---------------------------------------------------------------------------------------------
    // Lifecycle Methods
    // ---------------------------------------------------------------------------------------------

    @Override
    public NotificationViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_notification, parent, false);
        return new NotificationViewholder(view, mListener);
    }

    @Override
    public void onBindViewHolder(NotificationViewholder holder, int position) {
        holder.setItem(getItem(position));
    }

    // ---------------------------------------------------------------------------------------------
    // Items List Methods
    // ---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public NotificationInApp getItem(int position) {
        return mList.get(position);
    }

    public List<NotificationInApp> getItems() {
        return mList;
    }

    public void setItems(List<NotificationInApp> list) {
        mList.clear();
        mList = list;
        notifyDataSetChanged();
    }

    public void addPageItems(List<NotificationInApp> list) {
        int size = mList.size();
        mList.addAll(list);
        notifyItemRangeInserted(size, mList.size() - 1);
    }

    // ---------------------------------------------------------------------------------------------
    // Listeners Methods
    // ---------------------------------------------------------------------------------------------

    public void setOnClickListener(final NotificationViewholder.OnClickListener onClickListener) {
        mListener = onClickListener;
    }

    // ---------------------------------------------------------------------------------------------
    // TEST
    // ---------------------------------------------------------------------------------------------
    private void test() {
        mList.add(new NotificationInApp());
        mList.add(new NotificationInApp());
        mList.add(new NotificationInApp());
        mList.add(new NotificationInApp());
        mList.add(new NotificationInApp());
    }
}
