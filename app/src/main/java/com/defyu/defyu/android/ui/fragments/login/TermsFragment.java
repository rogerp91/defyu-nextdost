package com.defyu.defyu.android.ui.fragments.login;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link TermsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TermsFragment extends BaseFragment {
    private static final String TAG = TermsFragment.class.getSimpleName();

    public TermsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TermsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TermsFragment newInstance() {
        return new TermsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_terms;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_terms);
        setEnableBackToolbar(true);
    }

}
