package com.defyu.defyu.android.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.defyu.defyu.android.ui.fragments.BlankFragment;
import com.defyu.defyu.android.ui.fragments.myleagues.AllLeaguesFragment;
import com.defyu.defyu.android.ui.fragments.myleagues.MyLeaguesFragment;

/**
 * Created by dars_ on 21/12/2016.
 */

public class FooterAdapter extends FragmentPagerAdapter {
    private static final int MY_LEAGUES = 0;
    private static final int STATISTICS = 1;
    private static final int PRIZES = 2;
    private static final int ALL_LEAGUES = 3;

    public FooterAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case MY_LEAGUES:
                return MyLeaguesFragment.newInstance();
            case STATISTICS:
                return BlankFragment.newInstance("","");
            case PRIZES:
                return BlankFragment.newInstance("","");
            case ALL_LEAGUES:
                return AllLeaguesFragment.newInstance();
        }
        return BlankFragment.newInstance("","");
    }

    @Override
    public int getCount() {
        return 4;
    }
}
