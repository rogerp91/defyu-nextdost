package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.League;
import com.defyu.defyu.android.data.models.NotificationInApp;
import com.defyu.defyu.android.ui.viewholders.AllLeaguesViewholder;
import com.defyu.defyu.android.ui.viewholders.NotificationViewholder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com) on 17-12-2016.
 */

public class AllLeaguesAdapter extends RecyclerView.Adapter<AllLeaguesViewholder> {

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    private Context mContext;
    private List<League> mList;
    private AllLeaguesViewholder.OnClickListener mListener;

    // ---------------------------------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------------------------------

    public AllLeaguesAdapter(Context context) {
        mContext = context;
        mList = new ArrayList<>();
        test();
    }

    // ---------------------------------------------------------------------------------------------
    // Lifecycle Methods
    // ---------------------------------------------------------------------------------------------

    @Override
    public AllLeaguesViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_all_leagues, parent, false);
        return new AllLeaguesViewholder(view, mListener);
    }

    @Override
    public void onBindViewHolder(AllLeaguesViewholder holder, int position) {
        holder.setItem(getItem(position));
    }

    // ---------------------------------------------------------------------------------------------
    // Items List Methods
    // ---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public League getItem(int position) {
        return mList.get(position);
    }

    public List<League> getItems() {
        return mList;
    }

    public void setItems(List<League> list) {
        mList.clear();
        mList = list;
        notifyDataSetChanged();
    }

    public void addPageItems(List<League> list) {
        int size = mList.size();
        mList.addAll(list);
        notifyItemRangeInserted(size, mList.size() - 1);
    }

    // ---------------------------------------------------------------------------------------------
    // Listeners Methods
    // ---------------------------------------------------------------------------------------------

    public void setOnClickListener(final AllLeaguesViewholder.OnClickListener onClickListener) {
        mListener = onClickListener;
    }

    // ---------------------------------------------------------------------------------------------
    // TEST
    // ---------------------------------------------------------------------------------------------
    private int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private void test() {
        for (int i = 0, j = getRandomNumber(3, 6); i < j; i++) {
            mList.add(new League());
        }
    }
}
