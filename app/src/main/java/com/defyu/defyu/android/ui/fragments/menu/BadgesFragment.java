package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;


public class BadgesFragment extends BaseFragment {
    private static final String TAG = BadgesFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;

    public BadgesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BadgesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BadgesFragment newInstance() {
        return new BadgesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
        setHasOptionsMenu(true);
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_badges;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.badges);
        setEnableBackToolbar(true);

    }

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_notification, menu);
        menu.removeItem(R.id.menu_notification_on);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_notification:
                pushFragment(new NotificationsFragment());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}



