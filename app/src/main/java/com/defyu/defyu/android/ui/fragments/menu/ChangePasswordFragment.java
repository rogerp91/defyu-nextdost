package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.activities.BaseActivity;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.AndroidUtilities;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener, TextWatcher {
    private static final String TAG = ChangePasswordFragment.class.getSimpleName();

    @BindView(R.id.etActualPassword)
    EditText etActualPassword;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmNewPassword)
    EditText etConfirmNewPassword;

    @BindView(R.id.btnConfirm)
    Button btnConfirm;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ChangePasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance() {
        return new ChangePasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_change_password;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_chagne_password);
        setEnableBackToolbar(true);

        etActualPassword.addTextChangedListener(this);
        etNewPassword.addTextChangedListener(this);
        etConfirmNewPassword.addTextChangedListener(this);

        btnConfirm.setOnClickListener(this);
    }


    private boolean isValid(boolean toast) {
        if (AndroidUtilities.validateEmailEmpty(etActualPassword.getText())) {
            if (toast) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validatePassEmpty(etNewPassword.getText())) {
            if (toast) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }else if (AndroidUtilities.validatePassEmpty(etConfirmNewPassword.getText())) {
            if (toast) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }


        if(etActualPassword.getText().toString().contains(" ") || etNewPassword.getText().toString().contains(" ")
                || etConfirmNewPassword.getText().toString().contains(" ")){
            if (toast) {
                simpleToast(R.string.input_without_space);
            }
            return false;
        }


        if (!AndroidUtilities.validatePassLength(etActualPassword.getText())) {
            if (toast) {
                simpleToast(R.string.length_pass);
            }
            return false;
        }else if (!AndroidUtilities.validatePassLength(etNewPassword.getText())) {
            if (toast) {
                simpleToast(R.string.length_pass);
            }
            return false;
        }else if (!AndroidUtilities.validatePassLength(etConfirmNewPassword.getText())) {
            if (toast) {
                simpleToast(R.string.length_pass);
            }
            return false;
        }

        if(!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString())){
            if (toast) {
                simpleToast(R.string.Passwords_do_not_match);
            }
            return false;
        }
        return true;
    }


    private boolean isButtonInactive(boolean active) {
        if (AndroidUtilities.validateEmailEmpty(etActualPassword.getText())) {
            if (active) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validatePassEmpty(etNewPassword.getText())) {
            if (active) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }else if (AndroidUtilities.validatePassEmpty(etConfirmNewPassword.getText())) {
            if (active) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }
        return true;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnConfirm:
                if (!isValid(true)) {
                    return;
                }
                goBack(2);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        btnConfirm.setBackground(getResources().getDrawable(R.drawable.btn_green_transparent));
        btnConfirm.setTextColor(getResources().getColor(R.color.whiteTransparent));
        if (!isButtonInactive(false)) {
            return;
        }
        btnConfirm.setBackgroundResource(R.drawable.btn_green);
        btnConfirm.setTextColor(getResources().getColor(R.color.white));
    }
}
