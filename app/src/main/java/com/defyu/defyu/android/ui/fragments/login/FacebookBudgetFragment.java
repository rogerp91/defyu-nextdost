package com.defyu.defyu.android.ui.fragments.login;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.ui.activities.MainActivity;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

public class FacebookBudgetFragment extends BaseFragment implements View.OnClickListener{
    private static final String TAG = FacebookBudgetFragment.class.getSimpleName();

    private LoginActivityInterface mListener;
    private FragmentActivity context;

    private SharedPreferences ultimacaptura;
    private int permisoEscribirSD;

    @BindView(R.id.cancel)
    LinearLayout cancel;
    @BindView(R.id.share)
    LinearLayout share;
    @BindView(R.id.share_screen)
    LinearLayout RL;

    public FacebookBudgetFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FacebookBudgetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FacebookBudgetFragment newInstance() {
        return new FacebookBudgetFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_facebook_budget;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        ultimacaptura = getContext().getSharedPreferences("captura", Context.MODE_PRIVATE);

        cancel.setOnClickListener(this);
        share.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void borrarUltimaCaptura(){
        String url = ultimacaptura.getString("url","vacia");
        permisoEscribirSD = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (!url.equals("vacia")&&(permisoEscribirSD == PackageManager.PERMISSION_GRANTED))
            getContext().getContentResolver().delete(Uri.parse(url),null,null);
    }

    private void share(){
        permisoEscribirSD = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(permisoEscribirSD== PackageManager.PERMISSION_GRANTED) {
            ///////////////////////////////////captura de pantalla//////////////////////////////
            Bitmap bitmap = getScreenShot();
            String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bitmap, null, null);
            Log.i("share", path + "");
            borrarUltimaCaptura();
            SharedPreferences.Editor editor = ultimacaptura.edit();
            editor.putString("url", path);
            editor.commit();
            ////////////////////////////////////////código para compartir///////////////////////
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share) + ": " + getContext().getPackageName());
            intent.setType("image/*");
            Intent chooser = Intent.createChooser(intent, getString(R.string.share));
            if (intent.resolveActivity(getContext().getPackageManager()) != null)
                startActivity(chooser);
        }
    }

    private Bitmap getScreenShot(){
        RL.destroyDrawingCache();
        RL.setDrawingCacheEnabled(true);
        Bitmap bitmap= Bitmap.createBitmap(RL.getDrawingCache());
        return bitmap;
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        borrarUltimaCaptura();
        super.onResume();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.share:
                share();
                break;
        }

    }

}
