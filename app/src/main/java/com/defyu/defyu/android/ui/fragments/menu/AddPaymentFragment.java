package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

public class AddPaymentFragment extends BaseFragment implements View.OnClickListener{
    private static final String TAG = AddPaymentFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;

    @BindView(R.id.save_card)
    Button save_card;


    public AddPaymentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddPaymentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddPaymentFragment newInstance() {
        return new AddPaymentFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_add_payment;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.add_payment);
        setEnableBackToolbar(true);

        save_card.setOnClickListener(this);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_card:
                simpleToast(R.string.save_card);
                break;
        }
    }
}


