package com.defyu.defyu.android.ui.fragments.login;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.ui.adapters.OnboardingPagerAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link OnboardingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnboardingFragment extends BaseFragment implements ViewPager.OnPageChangeListener, View.OnClickListener {
    private static final String TAG = OnboardingFragment.class.getSimpleName();

    private OnboardingPagerAdapter mAdapter;

    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.cpIndicator)
    CirclePageIndicator cpIndicator;
    @BindView(R.id.btnLogin)
    TextView btnLogin;
    @BindView(R.id.btnJoin)
    TextView btnJoin;


    public OnboardingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment OnboardingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OnboardingFragment newInstance() {
        return new OnboardingFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_onboarding;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        mAdapter = new OnboardingPagerAdapter(getContext(),mPager);

        mPager.setAdapter(mAdapter);
        mPager.setOnPageChangeListener(cpIndicator);
        cpIndicator.setViewPager(mPager);
        cpIndicator.setOnPageChangeListener(this);
        btnJoin.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            return super.onBackPressed();
        } else {
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            return true;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnJoin:
                pushFragment(SignUpNormalFragment.newInstance());
                break;
            case R.id.btnLogin:
                Session.setFirstTime(false);
                pushFragment(LoginFragment.newInstance(), R.id.container, false);
                break;
        }
    }
}