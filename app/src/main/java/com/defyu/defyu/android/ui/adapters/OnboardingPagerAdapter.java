package com.defyu.defyu.android.ui.adapters;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;


public class OnboardingPagerAdapter extends PagerAdapter implements View.OnClickListener {
    private final static String TAG = OnboardingPagerAdapter.class.getSimpleName();

    private Context mContext;
    private ViewPager pager;


    private TextView tvTitle;
    private ImageView ivFigure;
    private LinearLayout llHowPlay;


    public OnboardingPagerAdapter(Context context, ViewPager pager) {
        mContext = context;
        this.pager = pager;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        View view = null;
        view = inflater.inflate(R.layout.item_pager_onbording, null);
        ivFigure = (ImageView) view.findViewById(R.id.ivFigure);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        llHowPlay = (LinearLayout) view.findViewById(R.id.llHowPlay);

        switch (position){
            case 0:
                tvTitle.setText(R.string.play_for_free);
                ivFigure.setImageResource(R.drawable.logo_completo_2);
                llHowPlay.setVisibility(View.VISIBLE);
                llHowPlay.setOnClickListener(this);
                break;
            case 1:
                tvTitle.setText(R.string.join_a_contest_and_pick);
                ivFigure.setImageResource(R.drawable.ic_players);
                llHowPlay.setVisibility(View.GONE);
                break;
            case 2:
                tvTitle.setText(R.string.follow_your_performance_live);
                ivFigure.setImageResource(R.drawable.ic_stat);
                llHowPlay.setVisibility(View.GONE);
                break;
            case 3:
                tvTitle.setText(R.string.earn_virtual_coins);
                ivFigure.setImageResource(R.drawable.ic_prize);
                llHowPlay.setVisibility(View.GONE);
                break;

        }
        ((ViewPager) container).addView(view, 0);
        return view;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout)object);
    }

    @Override
    public void onClick(View view) {
        if(pager != null){
            pager.setCurrentItem(pager.getCurrentItem()+1);
        }
    }
}
