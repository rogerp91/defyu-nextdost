package com.defyu.defyu.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.menu.EarnCoinsFragment;
import com.defyu.defyu.android.ui.fragments.myleagues.JoinLeagueFragment;
import com.defyu.defyu.android.ui.fragments.myleagues.MainPagerLeaguesFragment;
import com.defyu.defyu.android.ui.fragments.menu.LeaderboardFragment;
import com.defyu.defyu.android.ui.fragments.menu.NotificationsFragment;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.ui.fragments.menu.SettingsFragment;
import com.defyu.defyu.android.ui.fragments.myleagues.StandingsFragment;
import com.defyu.defyu.android.ui.fragments.search.SearchFragment;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements View.OnClickListener, MainActivityInterface {
    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navview)
    NavigationView navView;
    @BindView(R.id.toolbar)
    Toolbar appbar;

    ImageView go_facebook;
    ImageView go_twitter;
    ImageView go_instagram;
    LinearLayout hall_fame;
    LinearLayout leaderboard;
    LinearLayout earn_coins;
    LinearLayout settings;
    LinearLayout feedback;
    LinearLayout faq;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        setToolbar(appbar);
        setSupportActionBar(appbar);

        View hView = navView.getHeaderView(0);
        ImageView profile = (ImageView) hView.findViewById(R.id.profile);
        ImageView back = (ImageView) hView.findViewById(R.id.back);
        ImageView notification = (ImageView) hView.findViewById(R.id.notification);
        EditText search = (EditText) hView.findViewById(R.id.search);// Add Rp
        go_facebook = (ImageView) hView.findViewById(R.id.go_facebook);
        go_twitter = (ImageView) hView.findViewById(R.id.go_twitter);
        go_instagram = (ImageView) hView.findViewById(R.id.go_instagram);
        hall_fame = (LinearLayout) hView.findViewById(R.id.hall_fame);
        leaderboard = (LinearLayout) hView.findViewById(R.id.leaderboard);
        earn_coins = (LinearLayout) hView.findViewById(R.id.earn_coins);
        settings = (LinearLayout) hView.findViewById(R.id.settings);
        feedback = (LinearLayout) hView.findViewById(R.id.feedback);
        faq = (LinearLayout) hView.findViewById(R.id.faq);

        Glide.with(this).load("http://static.classora-technologies.com/files/uploads/images/entries/590852/main.jpg").into(profile);
        setupImageToolbar(R.drawable.ic_menu, true);

        back.setOnClickListener(this);
        notification.setOnClickListener(this);
        go_facebook.setOnClickListener(this);
        go_twitter.setOnClickListener(this);
        go_instagram.setOnClickListener(this);
        hall_fame.setOnClickListener(this);
        leaderboard.setOnClickListener(this);
        earn_coins.setOnClickListener(this);
        settings.setOnClickListener(this);
        feedback.setOnClickListener(this);
        faq.setOnClickListener(this);
        search.setOnClickListener(this);//Rp
        if (savedInstanceState == null) {
            pushFragment(MainPagerLeaguesFragment.newInstance(), R.id.container, false, TRANSACTION_WITHOUT_ANIMATION);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                if (fm.getBackStackEntryCount() == 0) {
                    drawerLayout.openDrawer(GravityCompat.START);
                    return true;
                }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                drawerLayout.closeDrawers();
                break;
            case R.id.notification:
                if (!getLastTagFragment().equals(NotificationsFragment.class.getSimpleName())) {
                    pushFragment(NotificationsFragment.newInstance());
                }
                drawerLayout.closeDrawers();
                break;
            case R.id.search:
                if (!getLastTagFragment().equals(SearchFragment.class.getSimpleName())) {
                    pushFragment(SearchFragment.newInstance());
                }
                drawerLayout.closeDrawers();
                break;
            case R.id.go_facebook:
                goFacebook();
                drawerLayout.closeDrawers();
                break;
            case R.id.go_twitter:
                goTwitter();
                drawerLayout.closeDrawers();
                break;
            case R.id.go_instagram:
                goInstagram();
                drawerLayout.closeDrawers();
                break;
            case R.id.hall_fame:
                simpleToast(R.string.hall_fame);
                drawerLayout.closeDrawers();
                break;
            case R.id.leaderboard:
                pushFragment(new LeaderboardFragment());
                drawerLayout.closeDrawers();
                break;
            case R.id.earn_coins:
                pushFragment(new EarnCoinsFragment());
                drawerLayout.closeDrawers();
                break;
            case R.id.settings:
                if (!getLastTagFragment().equals(SettingsFragment.class.getSimpleName())) {
                    pushFragment(SettingsFragment.newInstance());
                }
                drawerLayout.closeDrawers();
                break;
            case R.id.feedback:
                simpleToast("This feature is not available yet");
                drawerLayout.closeDrawers();
                break;
            case R.id.faq:
//                simpleToast("This feature is not available yet");
                pushFragment(new StandingsFragment());
                drawerLayout.closeDrawers();
                break;
        }
    }

    @Override
    public void logout() {
        Session.setUser(null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
