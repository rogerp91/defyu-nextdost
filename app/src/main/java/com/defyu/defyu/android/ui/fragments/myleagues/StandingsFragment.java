package com.defyu.defyu.android.ui.fragments.myleagues;


import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Standings;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.adapters.StandingsAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class StandingsFragment extends BaseFragment {
    private static final String TAG = StandingsFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;
    private StandingsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Standings> standingsList;

    @BindView(R.id.photo_player_1)
    ImageView photo_player_1;
    @BindView(R.id.photo_player_2)
    ImageView photo_player_2;
    @BindView(R.id.photo_player_3)
    ImageView photo_player_3;
    @BindView(R.id.name_player_1)
    TextView name_player_1;
    @BindView(R.id.name_player_2)
    TextView name_player_2;
    @BindView(R.id.name_player_3)
    TextView name_player_3;
    @BindView(R.id.points_player_1)
    TextView points_player_1;
    @BindView(R.id.points_player_2)
    TextView points_player_2;
    @BindView(R.id.points_player_3)
    TextView points_player_3;
    @BindView(R.id.list_position)
    RecyclerView list_position;
    @BindView(R.id.scContent)
    ScrollView scContent;
    @BindView(R.id.imPosition2)
    ImageView imPosition2;
    @BindView(R.id.imPosition1)
    ImageView imPosition1;
    @BindView(R.id.imPosition3)
    ImageView imPosition3;
    @BindView(R.id.spSeasons)
    Spinner spSeasons;

    public StandingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StandingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StandingsFragment newInstance() {
        return new StandingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_standings;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {

        recyclerStandings();

        imPosition1.setColorFilter(Color.rgb(122, 213, 102), PorterDuff.Mode.SRC_IN);
        imPosition2.setColorFilter(Color.rgb(142, 227, 194), PorterDuff.Mode.SRC_IN);
        imPosition3.setColorFilter(Color.rgb(198, 232, 138), PorterDuff.Mode.SRC_IN);


        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(),R.layout.item_standing,getResources().getStringArray(R.array.seasons)
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.item_standing);
        spSeasons.setAdapter(spinnerArrayAdapter);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void recyclerStandings(){

        initializeData();

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        list_position.setLayoutManager(mLayoutManager);
        mAdapter = new StandingsAdapter(standingsList);

        list_position.setAdapter(mAdapter);

        Glide.with(context).load(R.drawable.photo_testcopia_5).into(photo_player_1);
        Glide.with(context).load(R.drawable.photo_testcopia_5).into(photo_player_2);
        Glide.with(context).load(R.drawable.photo_testcopia_5).into(photo_player_3);

        name_player_1.setText("Lionel Messi");
        name_player_2.setText("Juan perez");
        name_player_3.setText("Bruno Diaz");
        points_player_1.setText(String.valueOf(98));
        points_player_2.setText(String.valueOf(89));
        points_player_3.setText(String.valueOf(87));

        list_position.setNestedScrollingEnabled(false);
    }

    private void initializeData(){
        standingsList = new ArrayList<>();
        standingsList.add(new Standings(4, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        standingsList.add(new Standings(5, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        standingsList.add(new Standings(6, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        standingsList.add(new Standings(7, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        standingsList.add(new Standings(8, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        standingsList.add(new Standings(9, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        standingsList.add(new Standings(10, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
    }

}


