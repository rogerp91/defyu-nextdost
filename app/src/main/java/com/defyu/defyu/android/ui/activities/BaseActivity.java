package com.defyu.defyu.android.ui.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.content.Context;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.widget.Toast;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.ButterKnife;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by dars_ on 9/12/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {
    public static final int TRANSACTION_WITHOUT_ANIMATION = 0;
    private final String TAG = BaseActivity.class.getSimpleName();

    public static String PROFILENAME = "defyugames";
    public static String FACEBOOK_URL = "https://www.facebook.com/defyugames";
    public static String TWITTER_URL = "https://twitter.com/defyugames";
    public static String INSTAGRAM_URL = "https://www.instagram.com/defyugames";

    protected FragmentManager fm;
    protected ArrayList<String> mTagFragments;
    private ProgressDialog progressDialog = null;
    private boolean haveToolbar;
    private Toolbar mToolbar;

    abstract public int getLayout();

    abstract public void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getSupportFragmentManager();
        mTagFragments = new ArrayList<>();
        setContentView(getLayout());
        ButterKnife.bind(this);
        onCreateView(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void setToolbar(Toolbar toolBar) {
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            mToolbar = toolBar;
            haveToolbar = true;
        }
    }

    @Override
    public void setTitle(int titleId) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleId);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    public void showToolbar() {
        if ((getSupportActionBar() != null) && haveToolbar) {
            setToolbar(mToolbar);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }

    public void hideToolbar() {
        if ((getSupportActionBar() != null) && haveToolbar) {
            mToolbar.setVisibility(View.GONE);
        }
    }

    public void setupImageToolbar(int resImage, boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(resImage);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        }
    }

    public void setEnableBackToolbar(boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            getSupportActionBar().setDisplayShowHomeEnabled(enable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            goBack();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (mTagFragments.size() > 0) {
            Fragment fragment = fm.findFragmentByTag(mTagFragments.get(mTagFragments.size() - 1));
            if (fragment instanceof BaseFragment) {
                BaseFragment base = (BaseFragment) fragment;
                if (base.onBackPressed()) {
                    return;
                }
            }
            mTagFragments.remove(mTagFragments.size() - 1);
        }
        super.onBackPressed();
    }

    public void goBack(int number) {
        if(number < 0){
            throw new RuntimeException("Error number back");
        }
        for (int i=0;i<number;i++){
            goBack();
        }
    }

    public void goBack() {
        if (mTagFragments.size() > 0) {
            mTagFragments.remove(mTagFragments.size() - 1);
            fm.popBackStackImmediate();
        } else {
            finish();
        }
    }

    public void pushFragment(Fragment fragment, int... animations) {
        pushFragment(fragment, R.id.container, true, animations);
    }

    public void pushFragment(Fragment fragment) {
        pushFragment(fragment, R.id.container, true);
    }

    public void pushFragment(Fragment fragment, int container, boolean addBackStack, int... animations) {
        FragmentTransaction transaction = fm.beginTransaction();
        String tag = fragment.getClass().getSimpleName();

        if (addBackStack) {
            transaction.addToBackStack(tag);
        }

        switch (animations.length) {
            case 0:
                transaction.setCustomAnimations(
                        R.anim.push_show_in_simple,
                        R.anim.push_hidden_out_simple,
                        0,
                        0);
                break;
            case 1:
                break;
            case 2:
                transaction.setCustomAnimations(animations[0], animations[1]);
                break;
            case 4:
                transaction.setCustomAnimations(animations[0], animations[1], animations[2], animations[3]);
                break;
            default:
                throw new RuntimeException("Error with animations transaction");
        }


        transaction.replace(container, fragment, tag);
        mTagFragments.add(tag);
        try {
            transaction.commit();
        } catch (Exception e) {
        }
    }

    public void showFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = fm.beginTransaction();

        transaction.addToBackStack(tag);
        transaction.add(fragment, tag);
        transaction.commitAllowingStateLoss();
    }

    public void showProgressDialog(int resMsg) {
        showProgressDialog(getString(R.string.app_name), getString(resMsg), false);
    }


    public void showProgressDialog(String msg) {
        showProgressDialog(getString(R.string.app_name), msg, false);
    }

    public void showProgressDialog(int resMsg, boolean cancelable) {
        showProgressDialog(getString(R.string.app_name), getString(resMsg), cancelable);
    }

    public void showProgressDialog(String msg, boolean cancelable) {
        showProgressDialog(getString(R.string.app_name), msg, cancelable);
    }

    public void showProgressDialog(String title, String msg, boolean cancelable) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(cancelable);
            progressDialog.setTitle(title);
            progressDialog.setMessage(msg);
            progressDialog.show();
        }
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void simpleToast(int resMsg) {
        Toast.makeText(this, resMsg, Toast.LENGTH_SHORT).show();
    }

    public void simpleToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    protected boolean isHorizontal() {
        boolean isHorizontal = false;
        int rotation = getWindowManager().getDefaultDisplay().getRotation();

        if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
            isHorizontal = true;
        }

        return isHorizontal;
    }


    public void rotate() {
        if (isHorizontal()) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        } else {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }
        }
        Log.d(TAG, "rotate");
    }

    public String getLastTagFragment() {
        if (mTagFragments.size() == 0) {
            return "";
        } else {
            return mTagFragments.get(mTagFragments.size() - 1);
        }
    }

    public void goFacebook() {
        try {
            int versionCode = this.getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + FACEBOOK_URL);
                this.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } else {
                Uri uri = Uri.parse("fb://page/" + PROFILENAME);
                this.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        } catch (PackageManager.NameNotFoundException e) {
            this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_URL)));
        }
    }

    public void goTwitter() {
        try {
            this.getPackageManager().getPackageInfo("com.twitter.android", 0);
            Uri uri = Uri.parse("twitter://user?screen_name=" + PROFILENAME);
            this.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (Exception e) {
            Uri uri = Uri.parse(TWITTER_URL);
            this.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }

    }

    public void goInstagram() {
        Uri uri = Uri.parse("http://instagram.com/_u/" + PROFILENAME);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            startActivity(likeIng);
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(INSTAGRAM_URL)));
        }
    }

    public BaseFragment findFragmentByTag(String tag){
        return (BaseFragment) fm.findFragmentByTag(tag);
    }
}
