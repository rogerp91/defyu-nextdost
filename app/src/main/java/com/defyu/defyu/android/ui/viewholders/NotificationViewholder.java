package com.defyu.defyu.android.ui.viewholders;

import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.NotificationInApp;
import com.defyu.defyu.android.ui.customersview.CircularImageView;

import atownsend.swipeopenhelper.BaseSwipeOpenViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com).
 */

public class NotificationViewholder extends BaseSwipeOpenViewHolder {

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    @BindView(R.id.container)
    public FrameLayout lyContainer;

    @BindView(R.id.foregroundView)
    public LinearLayout foregroundView;

    @BindView(R.id.endBackgroundView)
    public LinearLayout endBackgroundView;

    @BindView(R.id.avatar)
    public CircularImageView ivAvatar;

    @BindView(R.id.trash)
    public FrameLayout ivTrash;

    @BindView(R.id.share)
    public FrameLayout ivShare;

    @BindView(R.id.title)
    public TextView tvTitle;

    private NotificationInApp mNotification;
    private OnClickListener mlistener;
    private long mLastClickTime;

    // ---------------------------------------------------------------------------------------------
    // Copnstructors
    // ---------------------------------------------------------------------------------------------

    public NotificationViewholder(View view, OnClickListener listener) {
        super(view);
        ButterKnife.bind(this, view);
        mlistener = listener;
    }

    // ---------------------------------------------------------------------------------------------
    // Swipe Methods
    // ---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public View getSwipeView() {
        return foregroundView;
    }

    @Override
    public float getEndHiddenViewSize() {
        return endBackgroundView.getMeasuredWidth();
    }

    @Override
    public float getStartHiddenViewSize() {
        return 0;
    }

    // ---------------------------------------------------------------------------------------------
    // OnClick Methods
    // ---------------------------------------------------------------------------------------------

    @OnClick({R.id.foregroundView, R.id.trash, R.id.share})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.foregroundView:
                if (mlistener != null) {
                    mlistener.onItemClick(mNotification);
                }
                break;

            case R.id.trash:
                if (mlistener != null && isClickable()) {
                    mlistener.onRemove(mNotification);
                }
                break;

            case R.id.share:
                if (mlistener != null && isClickable()) {
                    mlistener.onShare(mNotification);
                }
                break;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Methods
    // ---------------------------------------------------------------------------------------------

    public void setItem(NotificationInApp notification) {
        mNotification = notification;
    }

    public boolean isClickable() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return false;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    // Listeners
    // ---------------------------------------------------------------------------------------------

    public interface OnClickListener {
        void onItemClick(NotificationInApp notification);

        void onRemove(NotificationInApp notification);

        void onShare(NotificationInApp notification);
    }
}
