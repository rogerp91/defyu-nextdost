package com.defyu.defyu.android.ui.fragments.menu;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link NotificationSettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationSettingsFragment extends BaseFragment {
    private static final String TAG = NotificationSettingsFragment.class.getSimpleName();

    public NotificationSettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NotificationSettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationSettingsFragment newInstance() {
        return new NotificationSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_notification_settings;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_notifications);
        setEnableBackToolbar(true);


    }
}
