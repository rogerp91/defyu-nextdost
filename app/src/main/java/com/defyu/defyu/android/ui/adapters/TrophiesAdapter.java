package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Badge;
import com.defyu.defyu.android.data.models.Trophy;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dars_ on 23/12/2016.
 */

public class TrophiesAdapter extends RecyclerView.Adapter<TrophiesAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Trophy> trophies;

    public TrophiesAdapter(Context context, List<Trophy> trophies) {
        mContext = context;
        this.trophies = trophies;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCoins)
        TextView tvCoins;
        @BindView(R.id.tvLeague)
        TextView tvLeague;
        @BindView(R.id.fmContent)
        FrameLayout fmContent;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public TrophiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.
                from(parent.getContext()).inflate(R.layout.item_tropy, parent, false);
        return new TrophiesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCoins.setText(String.valueOf(trophies.get(position).getCoints()));
        holder.tvLeague.setText(trophies.get(position).getLeague());
    }


    @Override
    public int getItemCount() {
        return trophies.size();
    }
}
