package com.defyu.defyu.android.ui.customersview;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.adapters.FooterAdapter;
import com.defyu.defyu.android.utils.AndroidUtilities;

import java.util.HashMap;

/**
 * Created by dars_ on 21/12/2016.
 */

public class FooterView extends FrameLayout implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private static final int MY_LEAGUES = 0;
    private static final int STATISTICS = 1;
    private static final int PRIZES = 2;
    private static final int ALL_LEAGUES = 3;
    private TextView tvItem1;
    private TextView tvItem2;
    private TextView tvItem3;
    private TextView tvItem4;

    private ImageView ivItem1;
    private ImageView ivItem2;
    private ImageView ivItem3;
    private ImageView ivItem4;

    private LinearLayout llMyLeague;
    private LinearLayout llStatistics;
    private LinearLayout llPrizes;
    private LinearLayout llAllLeagues;

    private ViewPager mViewPager;
    private FooterAdapter mAdapter;
    private OnTabChange onTabChange = null;

    private int icons[] = {
            R.drawable.ic_my_leagues,
            R.drawable.ic_statics,
            R.drawable.ic_prizes,
            R.drawable.ic_leagues
    };


    public FooterView(Context context) {
        super(context);
        init();
    }

    public FooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li =
                (LayoutInflater) getContext().getSystemService(infService);
        li.inflate(R.layout.view_footer, this, true);

        tvItem1 = (TextView) findViewById(R.id.tvItem1);
        tvItem2 = (TextView) findViewById(R.id.tvItem2);
        tvItem3 = (TextView) findViewById(R.id.tvItem3);
        tvItem4 = (TextView) findViewById(R.id.tvItem4);

        ivItem1 = (ImageView) findViewById(R.id.ivItem1);
        ivItem2 = (ImageView) findViewById(R.id.ivItem2);
        ivItem3 = (ImageView) findViewById(R.id.ivItem3);
        ivItem4 = (ImageView) findViewById(R.id.ivItem4);

        llMyLeague = (LinearLayout) findViewById(R.id.llMyLeague);
        llStatistics = (LinearLayout) findViewById(R.id.llStatistics);
        llPrizes = (LinearLayout) findViewById(R.id.llPrizes);
        llAllLeagues = (LinearLayout) findViewById(R.id.llAllLeagues);

        llMyLeague.setOnClickListener(this);
        llStatistics.setOnClickListener(this);
        llPrizes.setOnClickListener(this);
        llAllLeagues.setOnClickListener(this);
    }

    public void setupWithViewPager(@Nullable final ViewPager viewPager) {
        if (mViewPager != null) {
            // If we've already been setup with a ViewPager, remove us from it
            mViewPager.removeOnPageChangeListener(this);
        }

        if (viewPager != null) {
            final PagerAdapter adapter = viewPager.getAdapter();
            if (adapter == null) {
                throw new IllegalArgumentException("ViewPager does not have a PagerAdapter set");
            }
            mViewPager = viewPager;

            viewPager.addOnPageChangeListener(this);

            mAdapter = (FooterAdapter) adapter;
        } else {
            // We've been given a null ViewPager so we need to clear out the internal state,
            // listeners and observers
            mViewPager = null;
            mAdapter = null;
        }
    }

    void selectTab(int position) {
        selectTab(position, true);
    }

    void selectTab(int position, boolean updateIndicator) {

    }

    public void setOnTabChange(OnTabChange onTabChange){
        this.onTabChange = onTabChange;
    }
    public void removeOnTabChange(){
        this.onTabChange = null;
    }

    private void populateFromPagerAdapter() {
        if (mAdapter != null) {
            final int adapterCount = mAdapter.getCount();
            for (int i = 0; i < adapterCount; i++) {
                // addTab(newTab().setText(mPagerAdapter.getPageTitle(i)), false);
            }

            // Make sure we reflect the currently set ViewPager item
            if (mViewPager != null && adapterCount > 0) {
                final int curItem = mViewPager.getCurrentItem();
             /*   if (curItem != getSelectedTabPosition() && curItem < getTabCount()) {
                    selectTab(getTabAt(curItem));
                }*/
            }
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        AndroidUtilities.setColorIcon(getContext(), ivItem1, R.color.gray_tab_not_selected);
        AndroidUtilities.setColorIcon(getContext(), ivItem2, R.color.gray_tab_not_selected);
        AndroidUtilities.setColorIcon(getContext(), ivItem3, R.color.gray_tab_not_selected);
        AndroidUtilities.setColorIcon(getContext(), ivItem4, R.color.gray_tab_not_selected);

        tvItem1.setTextColor(getResources().getColor(R.color.gray_tab_not_selected));
        tvItem2.setTextColor(getResources().getColor(R.color.gray_tab_not_selected));
        tvItem3.setTextColor(getResources().getColor(R.color.gray_tab_not_selected));
        tvItem4.setTextColor(getResources().getColor(R.color.gray_tab_not_selected));
        switch (position) {
            case MY_LEAGUES:
                AndroidUtilities.setColorIcon(getContext(), ivItem1, R.color.white);
                tvItem1.setTextColor(getResources().getColor(R.color.white));
                break;
            case STATISTICS:
                AndroidUtilities.setColorIcon(getContext(), ivItem2, R.color.white);
                tvItem2.setTextColor(getResources().getColor(R.color.white));
                break;
            case PRIZES:
                AndroidUtilities.setColorIcon(getContext(), ivItem3, R.color.white);
                tvItem3.setTextColor(getResources().getColor(R.color.white));
                break;
            case ALL_LEAGUES:
                AndroidUtilities.setColorIcon(getContext(), ivItem4, R.color.white);
                tvItem4.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View view) {
        int position = 0;
        switch (view.getId()) {
            case R.id.llMyLeague:
                position = MY_LEAGUES;
                break;
            case R.id.llStatistics:
                position = STATISTICS;
                break;
            case R.id.llPrizes:
                position = PRIZES;
                break;
            case R.id.llAllLeagues:
                position = ALL_LEAGUES;
                break;
        }



        if(onTabChange != null){
            onTabChange.onTabSelected(position);
        }

        if((position != STATISTICS) && (position != PRIZES)){
            mViewPager.setCurrentItem(position);
            onPageSelected(position);
        }
    }

    public interface OnTabChange {
        void onTabSelected(int position);
    }


}
