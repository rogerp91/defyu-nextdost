package com.defyu.defyu.android.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.widget.EditText;
import android.widget.ImageView;


/**
 * Created by dars_ on 9/12/2016.
 */

public class AndroidUtilities extends Activity{
    /*
    *
    *  SHA1: 54:76:76:DC:35:3B:50:B9:85:78:3B:78:A5:FB:9F:E3:06:08:C5:AC
    *
    *  CLAVE FB: VHZ23DU7ULmFeDt4pfuf4wYIxaw=
    *
    * */


    public static boolean validateEmail(Editable email){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email.toString()).matches();
    }

    public static boolean validateEmailEmpty(Editable email){
        String strEmail = email.toString();
        return  strEmail.isEmpty();
    }

    public static boolean validatePassEmpty(Editable pass){
        String strPass = pass.toString();
        return  strPass.isEmpty();
    }

    public static boolean validateNameEmpty(Editable name){
        String strName = name.toString();
        return  strName.isEmpty();
    }

    public static boolean validatePassLength(Editable pass){
        String strPass = pass.toString();
        return  strPass.length() >= 6;
    }

    public static void setColorIcon(Context context, Drawable ivImage, int color) {
        int COLOR2 = context.getResources().getColor(color);
        PorterDuff.Mode mMode = PorterDuff.Mode.SRC_ATOP;
        ivImage.setColorFilter(COLOR2,mMode);
    }
    public static void setColorIcon(Context context, ImageView ivImage, int color) {
        if(ivImage == null){
            return;
        }
        int iColor = context.getResources().getColor(color);
        int red   = (iColor & 0xFF0000) / 0xFFFF;
        int green = (iColor & 0xFF00) / 0xFF;
        int blue  = iColor & 0xFF;

        float[] matrix = { 0, 0, 0, 0, red,
                0, 0, 0, 0, green,
                0, 0, 0, 0, blue,
                0, 0, 0, 1, 0 };

        ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
        ivImage.setColorFilter(colorFilter);
    }

}
