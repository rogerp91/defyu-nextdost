package com.defyu.defyu.android.ui.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.data.models.User;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.AndroidUtilities;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import butterknife.BindView;

public class LoginButtonsFragment extends BaseFragment implements View.OnClickListener, FacebookCallback<LoginResult>{
    private static final String TAG = LoginButtonsFragment.class.getSimpleName();

    private LoginActivityInterface mListener;
    private CallbackManager callbackManager;
    private FragmentActivity context;

    @BindView(R.id.sign_up)
    Button sign_up;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.login_button)
    LoginButton login_button;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;

    public LoginButtonsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginButtonsFragment newInstance() {
        return new LoginButtonsFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_login_buttons;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        login_button.setAlpha(0);
        login_button.setText("");

        ivLogo.setOnClickListener(this);
        sign_up.setOnClickListener(this);
        login.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logOut();

        login_button.setReadPermissions("public_profile", "email");
        // If using in a fragment
        login_button.setFragment(this);

        // Callback registration
        login_button.registerCallback(callbackManager, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void loginFacebook(Profile profile){
        Session.setUser(new User());
        pushFragment(FacebookBudgetFragment.newInstance());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                pushFragment(LoginFragment.newInstance());
                break;
            case R.id.ivLogo:
                Session.setFirstTime(true);
                break;
            case R.id.sign_up:
                pushFragment(SignUpNormalFragment.newInstance());
                break;
        }

    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Profile profile = Profile.getCurrentProfile();

        if(profile!=null){
            loginFacebook(profile);
        }else{
            ProfileTracker profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                    if (profile1 != null) {
                        loginFacebook(profile1);
                    }
                }
            };
            profileTracker.startTracking();
        }
    }

    @Override
    public void onCancel() {
        // App code
        simpleToast("onCancel");
    }

    @Override
    public void onError(FacebookException exception) {
        // App code
        simpleToast("onError");
    }
}
