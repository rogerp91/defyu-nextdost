package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.MyLeagues;

import java.util.List;

public class MyLeaguesAdapter extends RecyclerView.Adapter<MyLeaguesAdapter.LeaguesViewHolder> implements View.OnClickListener {
    List<MyLeagues> list;
    Context context;
    private View.OnClickListener listener;

    public static class LeaguesViewHolder extends RecyclerView.ViewHolder {
        TextView coins;
        TextView league;
        TextView price;
        TextView players;
        TextView rating;
        TextView deadline;

        LeaguesViewHolder(View itemView) {
            super(itemView);
            coins = (TextView)itemView.findViewById(R.id.coins);
            league = (TextView)itemView.findViewById(R.id.league);
            price = (TextView)itemView.findViewById(R.id.price);
            players = (TextView)itemView.findViewById(R.id.players);
            rating = (TextView)itemView.findViewById(R.id.rating);
            deadline = (TextView)itemView.findViewById(R.id.deadline);
        }
    }

    public MyLeaguesAdapter(List<MyLeagues> list){
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public LeaguesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_my_leagues, viewGroup, false);
        v.setOnClickListener(this);
        LeaguesViewHolder lvh = new LeaguesViewHolder(v);
        context = viewGroup.getContext();
        return lvh;
    }


    @Override
    public void onBindViewHolder(final LeaguesViewHolder holder, int i) {

        holder.coins.setText(list.get(i).getCoins());
        holder.league.setText(list.get(i).getLeague());
        holder.price.setText(list.get(i).getPrice());
        holder.players.setText(list.get(i).getPlayers());
        holder.rating.setText(list.get(i).getRating());
        holder.deadline.setText(list.get(i).getDeadline());

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public MyLeagues getItemPosition(int position){
        return list.get(position);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }
}