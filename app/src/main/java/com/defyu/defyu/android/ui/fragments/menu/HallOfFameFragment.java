package com.defyu.defyu.android.ui.fragments.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Badge;
import com.defyu.defyu.android.data.models.Trophy;
import com.defyu.defyu.android.ui.adapters.BadgesAdapter;
import com.defyu.defyu.android.ui.adapters.TrophiesAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link HallOfFameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HallOfFameFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = HallOfFameFragment.class.getSimpleName();

    private ArrayList<Trophy> trophiesList;
    private List<Badge> badgesList;
    private BadgesAdapter mBadgeAdapter;
    private TrophiesAdapter mTrophiesAdapter;

    @BindView(R.id.vwTrophiesAll)
    View vwTrophiesAll;
    @BindView(R.id.rvTrophies)
    RecyclerView rvTrophies;
    @BindView(R.id.vwBagessAll)
    View vwBagessAll;
    @BindView(R.id.rvBadges)
    RecyclerView rvBadges;

    public HallOfFameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HallOfFameFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HallOfFameFragment newInstance() {
        return new HallOfFameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeData();
        setHaveToolbar(true);
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_hall_of_fame;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_hall_of_fame);
        setEnableBackToolbar(true);
        recyclerTrophies();
        recyclerBadges();

        vwBagessAll.setOnClickListener(this);
        vwTrophiesAll.setOnClickListener(this);
    }

    private void initializeData(){
        trophiesList = new ArrayList<>();
        trophiesList.add(new Trophy("PRO LEAGUE",25));
        trophiesList.add(new Trophy("PRO LEAGUE",25));
        trophiesList.add(new Trophy("PRO LEAGUE",25));

        badgesList = new ArrayList<>();
        badgesList.add(new Badge(R.drawable.t_dcopia,"The Defiati","Lorem ipsum dolor sit amet."));
        badgesList.add(new Badge(R.drawable.t_fcopia,"The Fanatic","Lorem ipsum dolor sit amet."));
        badgesList.add(new Badge(R.drawable.forma_1_0_copia,"headhunter","Lorem ipsum dolor sit amet."));
    }

    private void recyclerTrophies(){
        StaggeredGridLayoutManager linearLayout = new StaggeredGridLayoutManager(1,
                StaggeredGridLayoutManager.HORIZONTAL);;
        rvTrophies.setLayoutManager(linearLayout);

        mTrophiesAdapter = new TrophiesAdapter(getContext(), trophiesList);
        rvTrophies.setAdapter(mTrophiesAdapter);
    }

    private void recyclerBadges(){
        LinearLayoutManager linearLayout = new LinearLayoutManager(getContext());
        rvBadges.setLayoutManager(linearLayout);

        mBadgeAdapter = new BadgesAdapter(getContext(), badgesList);
        rvBadges.setAdapter(mBadgeAdapter);
        rvBadges.setNestedScrollingEnabled(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.vwBagessAll:
                pushFragment(new BadgesFragment());
                break;
            case R.id.vwTrophiesAll:
                simpleToast("This feature is not available yet");
                break;
        }
    }
}
