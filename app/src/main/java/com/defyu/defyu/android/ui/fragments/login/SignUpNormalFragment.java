package com.defyu.defyu.android.ui.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.data.models.User;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.activities.MainActivity;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.AndroidUtilities;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainActivityInterface} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUpNormalFragment extends BaseFragment implements View.OnClickListener,
                                                                TextWatcher,
                                                                FacebookCallback<LoginResult> {
    private static final String TAG = LoginFragment.class.getSimpleName();

    private LoginActivityInterface mListener;
    private CallbackManager callbackManager;
    private FragmentActivity context;

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.full_name)
    EditText full_name;
    @BindView(R.id.your_email)
    EditText your_email;
    @BindView(R.id.create_password)
    EditText create_password;
    @BindView(R.id.confirm_password)
    EditText confirm_password;
    @BindView(R.id.terms_service)
    TextView terms_service;
    @BindView(R.id.privacy_policy)
    TextView privacy_policy;
    @BindView(R.id.sign_up)
    Button sign_up;
    @BindView(R.id.login_button)
    LoginButton login_button;


    public SignUpNormalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SignUpNormalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUpNormalFragment newInstance() {
        return new SignUpNormalFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_sign_up_normal;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        login_button.setAlpha(0);
        login_button.setText("");

        back.setOnClickListener(this);

        full_name.addTextChangedListener(this);
        your_email.addTextChangedListener(this);
        create_password.addTextChangedListener(this);
        confirm_password.addTextChangedListener(this);

        terms_service.setOnClickListener(this);
        privacy_policy.setOnClickListener(this);
        sign_up.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logOut();

        login_button.setReadPermissions("public_profile", "email");
        // If using in a fragment
        login_button.setFragment(this);



        // Callback registration
        login_button.registerCallback(callbackManager, this);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private boolean isValid(boolean toast) {
        if (AndroidUtilities.validateNameEmpty(full_name.getText())) {
            if(toast){
                simpleToast(R.string.fields_empty);
            }
            return false;
        }else if (AndroidUtilities.validatePassEmpty(create_password.getText())){
            if(toast){
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validatePassEmpty(confirm_password.getText())){
            if(toast){
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validateEmailEmpty(your_email.getText())){
            if(toast){
                simpleToast(R.string.fields_empty);
            }
            return false;
        }

        if (!AndroidUtilities.validateEmail(your_email.getText())) {
            if(toast){
                simpleToast(R.string.valid_email);
            }
            return false;
        }
        if (!AndroidUtilities.validatePassLength(create_password.getText())) {
            if(toast){
                simpleToast(R.string.length_pass);
            }
            return false;
        } else if (!AndroidUtilities.validatePassLength(confirm_password.getText())) {
            if(toast){
                simpleToast(R.string.length_pass);
            }
            return false;
        }
        return true;
    }

    private boolean isButtonInactive(boolean active) {
        if (AndroidUtilities.validateNameEmpty(full_name.getText())) {
            if(active){
                simpleToast(R.string.fields_empty);
            }
            return false;
        }else if (AndroidUtilities.validatePassEmpty(create_password.getText())){
            if(active){
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validatePassEmpty(confirm_password.getText())){
            if(active){
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validateEmailEmpty(your_email.getText())){
            if(active){
                simpleToast(R.string.fields_empty);
            }
            return false;
        }
        return true;
    }

    public void loginFacebook(Profile profile){
        simpleToast(R.string.success_fb);
        Session.setUser(new User());
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.terms_service:
                pushFragment(TermsFragment.newInstance());
                break;
            case R.id.privacy_policy:
                pushFragment(PrivacyPolicyFragment.newInstance());
                break;
            case R.id.login_button:
                simpleToast(R.string.sign_up_facebook);
                break;
            case R.id.back:
                goBack();
                break;
            case R.id.sign_up:

                if(!isValid(true)){
                    return;
                }
                //simpleToast(R.string.sign_up);
                pushFragment(CreateUsernameFragment.newInstance(full_name.getText().toString()));
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        //login.setText(R.string.login);
        sign_up.setBackground(getResources().getDrawable(R.drawable.btn_green_transparent));
        sign_up.setTextColor(getResources().getColor(R.color.whiteTransparent));
        if(!isButtonInactive(false)){
            return;
        }
        sign_up.setBackgroundResource(R.drawable.btn_green);
        sign_up.setTextColor(getResources().getColor(R.color.white));

    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Profile profile = Profile.getCurrentProfile();

        if(profile!=null){
            loginFacebook(profile);
        }else{
            ProfileTracker profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                    if (profile1 != null) {
                        loginFacebook(profile1);
                    }
                }
            };
            profileTracker.startTracking();
        }
    }

    @Override
    public void onCancel() {
        // App code
        simpleToast("onCancel");
    }

    @Override
    public void onError(FacebookException exception) {
        // App code
        simpleToast("onError");
    }
}

