package com.defyu.defyu.android.ui.fragments.menu;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.AndroidUtilities;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link PasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PasswordFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = PasswordFragment.class.getSimpleName();

    private boolean show = false;

    @BindView(R.id.tvShowPassword)
    TextView tvShowPassword;
    @BindView(R.id.btnChangePassword)
    Button btnChangePassword;
    @BindView(R.id.etPassword)
    EditText etPassword;

    public PasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PasswordFragment newInstance() {
        return new PasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_password;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_password);
        setEnableBackToolbar(true);

        btnChangePassword.setOnClickListener(this);
        tvShowPassword.setOnClickListener(this);
    }

    private boolean isValid(boolean toast) {

        if (AndroidUtilities.validatePassEmpty(etPassword.getText())) {
            if (toast) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }
        if (!AndroidUtilities.validatePassLength(etPassword.getText())) {
            if (toast) {
                simpleToast(R.string.length_pass);
            }
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnChangePassword:
                if (!isValid(true)) {
                    return;
                }
                pushFragment(ChangePasswordFragment.newInstance());
                break;
            case R.id.tvShowPassword:
                if(show){
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    tvShowPassword.setText(R.string.hide_password);
                }else{
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    tvShowPassword.setText(R.string.show_password);
                }
                show = !show;
                break;
        }
    }
}
