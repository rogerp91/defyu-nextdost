package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.ui.fragments.myleagues.AllLeaguesFragment;

import butterknife.BindView;

public class EarnCoinsFragment extends BaseFragment implements View.OnClickListener{
    private static final String TAG = EarnCoinsFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;

    @BindView(R.id.share)
    Button share;
    @BindView(R.id.buy_defyu)
    LinearLayout buy_defyu;
    @BindView(R.id.win_league)
    LinearLayout win_league;
    @BindView(R.id.win_badges)
    LinearLayout win_badges;

    public EarnCoinsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EarnCoinsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EarnCoinsFragment newInstance() {
        return new EarnCoinsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
        setHasOptionsMenu(true);
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_earn_coins;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.earn_coins);
        setEnableBackToolbar(true);

        share.setOnClickListener(this);
        buy_defyu.setOnClickListener(this);
        win_league.setOnClickListener(this);
        win_badges.setOnClickListener(this);

    }

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_info, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.info:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void shareDefyu(){
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
            String sAux = getString(R.string.share) + ": " ;
            sAux = sAux + getContext().getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch(Exception e) {
            //e.toString();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.share:
                shareDefyu();
                break;
            case R.id.buy_defyu:
                simpleToast(R.string.shop);
                break;
            case R.id.win_league:
                pushFragment(new AllLeaguesFragment());
                break;
            case R.id.win_badges:
                simpleToast(R.string.view_badges);
                break;
        }
    }
}


