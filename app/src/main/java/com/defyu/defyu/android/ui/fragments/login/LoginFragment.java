package com.defyu.defyu.android.ui.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.data.models.User;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.activities.MainActivity;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.AndroidUtilities;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import butterknife.BindView;


/**
 * A simple {@link BaseFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainActivityInterface} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener, TextWatcher,
        FacebookCallback<LoginResult> {
    private static final String TAG = LoginFragment.class.getSimpleName();

    private LoginActivityInterface mListener;
    private CallbackManager callbackManager;
    private FragmentActivity context;

    @BindView(R.id.enter_email)
    EditText enter_email;
    @BindView(R.id.enter_password)
    EditText enter_password;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.forgot_pass)
    TextView forgot_pass;
    @BindView(R.id.login_button)
    LoginButton login_button;
    @BindView(R.id.back)
    ImageView back;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        login_button.setAlpha(0);
        login_button.setText("");

        back.setOnClickListener(this);

        enter_email.addTextChangedListener(this);
        enter_password.addTextChangedListener(this);
        forgot_pass.setOnClickListener(this);
        login.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logOut();

        login_button.setReadPermissions("public_profile", "email");
        // If using in a fragment
        login_button.setFragment(this);

        // Callback registration
        login_button.registerCallback(callbackManager, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private boolean isValid(boolean toast) {
        if (AndroidUtilities.validateEmailEmpty(enter_email.getText())) {
            if (toast) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validatePassEmpty(enter_password.getText())) {
            if (toast) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }
        if(enter_email.getText().toString().contains(" ")){
            simpleToast(R.string.input_without_space);
            return false;
        }
        if (!AndroidUtilities.validatePassLength(enter_password.getText())) {
            if (toast) {
                simpleToast(R.string.length_pass);
            }
            return false;
        }
        return true;
    }

    private boolean isButtonInactive(boolean active) {
        if (AndroidUtilities.validateEmailEmpty(enter_email.getText())) {
            if (active) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        } else if (AndroidUtilities.validatePassEmpty(enter_password.getText())) {
            if (active) {
                simpleToast(R.string.fields_empty);
            }
            return false;
        }
        return true;
    }

    public void loginFacebook(Profile profile) {
        Session.setUser(new User());
        pushFragment(FacebookBudgetFragment.newInstance());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_pass:
                pushFragment(ForgotPasswordFragment.newInstance());
                break;
            case R.id.login:

                if (!isValid(true)) {
                    return;
                }
                Session.setUser(new User());
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);

                getActivity().finish();
                break;
            case R.id.back:
                goBack();
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        //login.setText(R.string.login);
        login.setBackground(getResources().getDrawable(R.drawable.btn_green_transparent));
        login.setTextColor(getResources().getColor(R.color.whiteTransparent));
        if (!isButtonInactive(false)) {
            return;
        }
        login.setBackgroundResource(R.drawable.btn_green);
        login.setTextColor(getResources().getColor(R.color.white));

    }


    @Override
    public void onSuccess(LoginResult loginResult) {
        Profile profile = Profile.getCurrentProfile();

        if (profile != null) {
            loginFacebook(profile);
        } else {
            ProfileTracker profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                    if (profile1 != null) {
                        loginFacebook(profile1);
                    }
                }
            };
            profileTracker.startTracking();
        }
    }

    @Override
    public void onCancel() {
        // App code
        simpleToast("onCancel");
    }

    @Override
    public void onError(FacebookException exception) {
        // App code
        simpleToast("onError");
    }
}
