package com.defyu.defyu.android.ui.fragments.menu;


import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com).
 */
public class SettingsFragment extends BaseFragment {

    // ---------------------------------------------------------------------------------------------
    // Constansts
    // ---------------------------------------------------------------------------------------------

    private static final String TAG = SettingsFragment.class.getSimpleName();

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    @BindView(R.id.profile)
    ImageView profile;

    @BindView(R.id.editPhoto)
    TextView tvEditPhoto;

    @BindView(R.id.personalInfo)
    TextView tvPersonalInfo;

    @BindView(R.id.notifications)
    TextView tvNotifications;

    @BindView(R.id.about)
    TextView tvAbout;

    private long mLastClickTime;
    private MainActivityInterface mListener;

    // ---------------------------------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------------------------------

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
        //setHasOptionsMenu(false);
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setEnableBackToolbar(true);
        setTitle(getString(R.string.settings_title));

        Glide.with(this).load("http://static.classora-technologies.com/files/uploads/images/entries/590852/main.jpg").into(profile);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityInterface) {
            mListener = (MainActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MainActivityInterface");
        }
    }

    // ---------------------------------------------------------------------------------------------
    // OnCLick Methods
    // ---------------------------------------------------------------------------------------------

    @OnClick({R.id.editPhoto, R.id.personalInfo, R.id.notifications, R.id.about, R.id.logout})
    public void onClick(View view) {
        if (!isClickable()) return;

        switch (view.getId()) {
            case R.id.editPhoto:
                Toast.makeText(getContext(), "This feature is not available yet", Toast.LENGTH_SHORT).show();
                break;
            case R.id.personalInfo:
                pushFragment(new PersonalInfoFragment());
                break;
            case R.id.notifications:
                pushFragment(NotificationSettingsFragment.newInstance());
                break;

            case R.id.about:
                pushFragment(AboutFragment.newInstance());
                break;

            case R.id.logout:
                mListener.logout();
                break;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Methods
    // ---------------------------------------------------------------------------------------------

    public boolean isClickable() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return false;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        return true;
    }
}
