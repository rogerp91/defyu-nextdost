package com.defyu.defyu.android.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.ui.activities.BaseActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * Created by dars_ on 9/12/2016.
 */

public abstract class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();
    public static final int TRANSACTION_WITHOUT_ANIMATION = 0;

    private BaseActivity activity;
    private Map<String, String> permissionDescriptions;
    private boolean haveToolbar;
    private Toolbar mToolbar;
    private int resToolbar = 0;

    abstract public int getLayoutView();

    abstract public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                                     @Nullable Bundle savedInstanceState, View view);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (BaseActivity) getActivity();
        permissionDescriptions = new HashMap<>();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = (BaseActivity) getActivity();
        permissionDescriptions = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getLayoutView(), container, false);
        ButterKnife.bind(this, view);
        if (haveToolbar) {
            onCreateToolbar(view, resToolbar, null);
        }
        onViewReady(inflater, container, savedInstanceState, view);
        return view;
    }

    public void onCreateToolbar(View view, int resToolbar, Toolbar toolbar) {
        if (toolbar == null) {
            try {
                if (resToolbar == 0) {
                    resToolbar = R.id.toolbar;
                }
                mToolbar = (Toolbar) view.findViewById(resToolbar);
                setToolBar(mToolbar);
                activity.hideToolbar();
                haveToolbar = true;
            } catch (Exception e) {
                haveToolbar = false;
            }
        } else {
            mToolbar = toolbar;
            haveToolbar = true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (haveToolbar) {
            activity.hideToolbar();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (haveToolbar && !haveToolbarLastFragment()) {
            activity.showToolbar();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    public void setHaveToolbar(boolean haveToolbar, int resToolbar) {
        this.resToolbar = resToolbar;
        setHaveToolbar(haveToolbar);
    }

    public void setHaveToolbar(boolean haveToolbar) {
        this.haveToolbar = haveToolbar;
    }

    protected void showProgressDialog(String msg) {
        activity.showProgressDialog(msg);
    }

    protected void showProgressDialog(int resMsg) {
        activity.showProgressDialog(getString(resMsg));
    }

    public void simpleToast(int resMsg) {
        activity.simpleToast(resMsg);
    }

    public void simpleToast(String msg) {
        activity.simpleToast(msg);
    }

    protected void pushFragment(Fragment fragment, int... animations) {
        activity.pushFragment(fragment, animations);
    }

    protected void pushFragment(Fragment fragment) {
        activity.pushFragment(fragment);
    }

    protected void pushFragment(Fragment fragment, int container, boolean addBackStack,
                                int... animations) {
        activity.pushFragment(fragment, container, addBackStack, animations);
    }

    protected void pushFragment(Fragment fragment, int container, boolean addBackStack) {
        activity.pushFragment(fragment, container, addBackStack);
    }

    protected void showProgressDialog(String title, int msgResId) {
        showProgressDialog(title, getString(msgResId));
    }

    protected void showProgressDialog(String title, String msg) {
        activity.showProgressDialog(title, msg, false);
    }

    protected void dismissDialog() {
        if (activity != null) {
            activity.dismissDialog();
        }
    }

    protected void showFragment(DialogFragment fragment) {
        fragment.show(getFragmentManager(), fragment.getClass().getSimpleName());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        boolean granted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        String permission = permissions[0];
    }

    /**
     * Check if the required permission is granted, or not. If the permission is not granted yet,
     * then request it.
     *
     * @param permission The permission name
     * @return true|false;
     */
    protected boolean hasPermission(String permission) {
        boolean granted = ContextCompat.checkSelfPermission(getContext(), permission) ==
                PackageManager.PERMISSION_GRANTED;

        if (!granted) {
            requestPermission(permission);
        }

        return granted;
    }

    protected void setTitle(int resId) {
        setTitle(getContext().getString(resId));
    }

    protected void setTitle(String title) {
        if (haveToolbar) {
            mToolbar.setTitle(title);
        } else {
            activity.setTitle(title);
        }
    }

    protected void requestPermission(final String permission) {

        // Should we show an explanation?
        if (shouldShowRequestPermissionRationale(permission)) {
            String text = permissionDescriptions.get(permission);

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    requestPermissions(new String[]{permission}, 0);
                }

            };

            showMessageOKCancel(text, listener);
        } else {
            requestPermissions(new String[]{permission}, 0);
        }

    }

    public boolean onBackPressed() {
        return false;
    }

    protected void goBack(int number) {
        activity.goBack(number);
    }

    protected void goBack() {
        activity.goBack();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(R.string.ok, okListener)
                .setNegativeButton(R.string.cancel, null)
                .create()
                .show();
    }

    public void setToolBar(Toolbar toolBar) {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        if (baseActivity != null) {
            mToolbar = toolBar;
            haveToolbar = true;
            ((BaseActivity) getActivity()).setSupportActionBar(toolBar);
        }
    }

    public void setEnableBackToolbar(boolean enable) {
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            activity.getSupportActionBar().setDisplayShowHomeEnabled(enable);
        }
    }

    public void startActivity(Intent intent) {
        activity.startActivity(intent);
    }

    public String getLastTagFragment() {
        return activity.getLastTagFragment();
    }

    public boolean haveToolbarLastFragment() {
        String tag = getLastTagFragment();
        return !tag.isEmpty() && activity.findFragmentByTag(tag).haveToolbar;
    }


}
