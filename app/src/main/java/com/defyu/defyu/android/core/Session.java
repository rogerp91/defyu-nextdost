package com.defyu.defyu.android.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.defyu.defyu.android.data.models.User;
import com.google.gson.Gson;

/**
 * Created by dars_ on 9/12/2016.
 */

public class Session {
    private static final String ENDPOINT_PROD = "";
    private static final String ENDPOINT_DEV = "";

    private static final String SESSION_PREFERENCES = "sessionData";
    private static final String KEY_CURRENT_USER = "currentUser";
    private static final String KEY_FIRST_TIME = "firstTime";
    private static final String KEY_FIRST_TIME_MY_LEAGUES = "firstTimeMyLeagues";
    private static final String KEY_REMEMBER = "remember";

    private static User mUser;
    private static SharedPreferences preferences;


    public static void init(Context context) {
        preferences = context.getApplicationContext().getSharedPreferences(SESSION_PREFERENCES,
                Context.MODE_PRIVATE);

        Gson gson = new Gson();

        // Load last logged in user.
        String json = preferences.getString(KEY_CURRENT_USER, null);

        if (json != null) {
            mUser = gson.fromJson(json, User.class);
        }
    }

    private static void save(String key, Object value) {
        SharedPreferences.Editor editor = preferences.edit();

        if (value instanceof String) {
            editor.putString(key, value.toString());
        }else {
            editor.putString(key, new Gson().toJson(value));
        }
        editor.apply();
    }
    private static void save (String key, boolean value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }

    public static boolean isUserLogin() {
        return mUser != null;
    }

    public static boolean isFirstTime(){
        return preferences.getBoolean(KEY_FIRST_TIME, true);
    }

    public static void setFirstTime(boolean firstTime){
        save(KEY_FIRST_TIME, firstTime);
    }


    public static boolean isFirstTimeMyLeague(){
        return preferences.getBoolean(KEY_FIRST_TIME_MY_LEAGUES, true);
    }

    public static void setKeyFirstTimeMyLeagues(boolean firstTime){
        save(KEY_FIRST_TIME_MY_LEAGUES, firstTime);
    }

    public static void setUser(User user) {
        Session.mUser = user;
        save(KEY_CURRENT_USER, user);
    }
}
