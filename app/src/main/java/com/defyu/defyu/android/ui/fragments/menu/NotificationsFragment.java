package com.defyu.defyu.android.ui.fragments.menu;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.NotificationInApp;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.adapters.NotificationAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.ui.viewholders.NotificationViewholder;

import atownsend.swipeopenhelper.SwipeOpenItemTouchHelper;
import butterknife.BindView;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com).
 */
public class NotificationsFragment extends BaseFragment {

    // ---------------------------------------------------------------------------------------------
    // Constansts
    // ---------------------------------------------------------------------------------------------

    private static final String TAG = NotificationsFragment.class.getSimpleName();

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    private SwipeOpenItemTouchHelper mRecyclerSwipeHelper;
    private NotificationAdapter mAdapter;
    private MainActivityInterface mListener;

    // ---------------------------------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------------------------------

    public NotificationsFragment() {
        // Required empty public constructor
    }

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }

    // ---------------------------------------------------------------------------------------------
    // Lifecycle
    // ---------------------------------------------------------------------------------------------

    @Override
    public int getLayoutView() {
        return R.layout.fragment_notifications;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.title_notifications);
        setEnableBackToolbar(true);
        if (mAdapter == null) {
            mAdapter = new NotificationAdapter(getContext());
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        mAdapter.setOnClickListener(new NotificationViewholder.OnClickListener() {
            @Override
            public void onItemClick(NotificationInApp notification) {
                mRecyclerSwipeHelper.closeAllOpenPositions();
            }

            @Override
            public void onRemove(NotificationInApp notification) {
                Toast.makeText(getContext(), "Remove option is not available yet", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onShare(NotificationInApp notification) {
                Toast.makeText(getContext(), "Share option is not available yet", Toast.LENGTH_SHORT).show();
            }
        });

        rvNotifications.setHasFixedSize(true);
        rvNotifications.setLayoutManager(linearLayoutManager);
        rvNotifications.setAdapter(mAdapter);

        mRecyclerSwipeHelper = new SwipeOpenItemTouchHelper(new SwipeOpenItemTouchHelper
                .SimpleCallback(SwipeOpenItemTouchHelper.START | SwipeOpenItemTouchHelper.END));

        mRecyclerSwipeHelper.attachToRecyclerView(rvNotifications);
        mRecyclerSwipeHelper.setCloseOnAction(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityInterface) {
            mListener = (MainActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_notification, menu);
        menu.removeItem(R.id.menu_notification_on);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                simpleToast("notification");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
