package com.defyu.defyu.android.ui.fragments.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;
import com.defyu.defyu.android.utils.AndroidUtilities;

import butterknife.BindView;

/**
 * Created by Mariexi on 13/12/16.
 */
public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener, TextWatcher {
    private static final String TAG = LoginFragment.class.getSimpleName();

    private LoginActivityInterface mListener;
    private FragmentActivity context;

    @BindView(R.id.enter_email_forgot)
    EditText enter_email_forgot;
    @BindView(R.id.reset_password)
    Button reset_password;
    @BindView(R.id.back)
    ImageView back;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ForgotPasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        enter_email_forgot.addTextChangedListener(this);
        reset_password.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private boolean isValid(boolean toast) {
        if (AndroidUtilities.validateEmailEmpty(enter_email_forgot.getText())) {
            if(toast){
                simpleToast(R.string.fields_empty);
            }
            return false;
        }
        if (!AndroidUtilities.validateEmail(enter_email_forgot.getText())) {
            if(toast){
                simpleToast(R.string.valid_email);
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reset_password:

                if(!isValid(true)){
                    return;
                }

                simpleToast(R.string.email_send);
                break;
            case R.id.back:
                goBack();
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        reset_password.setBackground(getResources().getDrawable(R.drawable.btn_green_transparent));
        reset_password.setTextColor(getResources().getColor(R.color.whiteTransparent));
        if (AndroidUtilities.validatePassEmpty(enter_email_forgot.getText())){
            return;
        }
        reset_password.setBackgroundResource(R.drawable.btn_green);
        reset_password.setTextColor(getResources().getColor(R.color.white));
    }
}
