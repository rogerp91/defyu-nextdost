package com.defyu.defyu.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.core.Session;
import com.defyu.defyu.android.ui.fragments.login.SplashFragment;

public class LaunchActivity extends BaseActivity {

    private static final long SECOND_DELAY = 3000;

    @Override
    public int getLayout() {
        return R.layout.activity_launch;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        if(Session.isUserLogin()){
            //MAIN
            Intent intent;
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }else{
            //LOGIN
            pushFragment(SplashFragment.newInstance(),R.id.container, false);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(LaunchActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SECOND_DELAY);
        }
    }

}
