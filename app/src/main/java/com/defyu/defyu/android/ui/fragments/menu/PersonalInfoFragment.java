package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

public class PersonalInfoFragment extends BaseFragment implements View.OnClickListener{
    private static final String TAG = PersonalInfoFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;

    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.full_name)
    TextView full_name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.password)
    TextView password;
    @BindView(R.id.language)
    EditText language;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.payment)
    EditText payment;


    public PersonalInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PersonalInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonalInfoFragment newInstance() {
        return new PersonalInfoFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_personal_info;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.settings_personal_info);
        setEnableBackToolbar(true);

        language.setText("ESP");
        password.setText("123456");
        email.setText("javier@mascherano.com");
        full_name.setText("Javier Jose Mascherano J.");
        username.setText("@Jmasche458");
        phone.setText("+12345678910");

        address.setOnClickListener(this);
        payment.setOnClickListener(this);
        language.setOnClickListener(this);
        password.setOnClickListener(this);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address:
                pushFragment(AddressFragment.newInstance());
                break;
            case R.id.payment:
                pushFragment(new AddPaymentFragment());
                break;
            case R.id.language:
                simpleToast("Change languaje");
                break;
            case R.id.password:
                pushFragment(PasswordFragment.newInstance());
                break;
        }
    }
}

