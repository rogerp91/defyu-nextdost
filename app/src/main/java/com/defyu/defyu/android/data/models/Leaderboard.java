package com.defyu.defyu.android.data.models;

public class Leaderboard {
    int position;
    int photo;
    String full_name;
    int points;

    public Leaderboard(int position, int photo, String full_name, int points) {
        this.position = position;
        this.photo = photo;
        this.full_name = full_name;
        this.points = points;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


}
