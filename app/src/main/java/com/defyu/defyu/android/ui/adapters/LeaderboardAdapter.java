package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Leaderboard;

import java.util.List;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.LeaderboardViewHolder> {
    List<Leaderboard> list;
    Context context;
    private View.OnClickListener listener;

    public static class LeaderboardViewHolder extends RecyclerView.ViewHolder {
        TextView position;
        ImageView photo;
        TextView full_name;
        TextView points;
        View vwLast;

        LeaderboardViewHolder(View itemView) {
            super(itemView);
            position = (TextView)itemView.findViewById(R.id.position);
            photo = (ImageView) itemView.findViewById(R.id.photo);
            full_name = (TextView)itemView.findViewById(R.id.full_name);
            points = (TextView)itemView.findViewById(R.id.points);
            vwLast = itemView.findViewById(R.id.vwLast);
        }
    }

    public LeaderboardAdapter(List<Leaderboard> list){
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public LeaderboardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_leaderboard, viewGroup, false);
        LeaderboardViewHolder lvh = new LeaderboardViewHolder(v);
        context = viewGroup.getContext();
        return lvh;
    }


    @Override
    public void onBindViewHolder(final LeaderboardViewHolder holder, int i) {
        Glide.with(context)
                .load(list.get(i).getPhoto())
                .into(holder.photo);
        holder.position.setText(String.valueOf(list.get(i).getPosition()));
        holder.full_name.setText(list.get(i).getFull_name());
        holder.points.setText(String.valueOf(list.get(i).getPoints()));

        holder.vwLast.setVisibility((i == (list.size()-1))? View.VISIBLE: View.GONE);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Leaderboard getItemPosition(int position){
        return list.get(position);
    }

}
