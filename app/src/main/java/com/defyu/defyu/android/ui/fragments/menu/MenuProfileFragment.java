package com.defyu.defyu.android.ui.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.interfaces.LoginActivityInterface;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import butterknife.BindView;

public class MenuProfileFragment extends BaseFragment implements View.OnClickListener{
    private static final String TAG = MenuProfileFragment.class.getSimpleName();

    private LoginActivityInterface mListener;
    private FragmentActivity context;

//    @BindView(R.id.enter_email_forgot)
//    EditText enter_email_forgot;
//    @BindView(R.id.reset_password)
//    Button reset_password;
    @BindView(R.id.profile)
    ImageView profile;
    @BindView(R.id.notification)
    ImageView notification;

    public MenuProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MenuProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuProfileFragment newInstance() {
        return new MenuProfileFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_menu_profile;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        Glide.with(this).load("http://static.classora-technologies.com/files/uploads/images/entries/590852/main.jpg").into(profile);
        notification.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof LoginActivityInterface) {
            mListener = (LoginActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.reset_password:
//                simpleToast(R.string.email_send);
//                break;
//            case R.id.back:
//                goBack();
//                break;
            case R.id.notification:
                if(!getLastTagFragment().equals(NotificationsFragment.class.getSimpleName())){
                    pushFragment(NotificationsFragment.newInstance());
                }
                break;
        }

    }

}
