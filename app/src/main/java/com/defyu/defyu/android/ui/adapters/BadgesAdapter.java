package com.defyu.defyu.android.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Badge;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dars_ on 23/12/2016.
 */

public class BadgesAdapter extends RecyclerView.Adapter<BadgesAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Badge> badgesList;

    public BadgesAdapter(Context context, List<Badge> badgeList) {
        mContext = context;
        this.badgesList = badgeList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivBadge)
        ImageView ivBadge;
        @BindView(R.id.tvtitle)
        TextView tvtitle;
        @BindView(R.id.tvSubtitle)
        TextView tvSubtitle;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public BadgesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.
                from(parent.getContext()).inflate(R.layout.item_badge, parent, false);
        return new BadgesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ivBadge.setImageResource(badgesList.get(position).getIcon());
        holder.tvtitle.setText(badgesList.get(position).getTitle());
        holder.tvSubtitle.setText(badgesList.get(position).getSubTitle());
    }


    @Override
    public int getItemCount() {
        return badgesList.size();
    }
}
