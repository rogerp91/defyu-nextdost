package com.defyu.defyu.android.data.models;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com).
 */

public class League {

    private String leauge;//Add Rp

    public League() {
    }

    public League(String leauge) {
        this.leauge = leauge;
    }

    public String getLeauge() {
        return leauge;
    }

    public void setLeauge(String leauge) {
        this.leauge = leauge;
    }
}
