package com.defyu.defyu.android.ui.viewholders;

import android.content.Context;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.League;
import com.defyu.defyu.android.ui.customersview.CircularImageView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Deneb Chorny (denebchorny@gmail.com).
 */

public class AllLeaguesViewholder extends RecyclerView.ViewHolder {

    // ---------------------------------------------------------------------------------------------
    // Variables
    // ---------------------------------------------------------------------------------------------

    @BindView(R.id.parent)
    public RelativeLayout rlPrent;

    @BindView(R.id.prize)
    public ImageView ivPrize;

    @BindView(R.id.team1)
    public ImageView ivTeam1;

    @BindView(R.id.team2)
    public ImageView ivTeam2;

    @BindView(R.id.participants)
    public TextView tvParticipants;

    @BindView(R.id.calendar)
    public TextView tvCalendar;

    @BindView(R.id.join)
    public TextView tvJoin;

    @BindView(R.id.credits)
    public TextView tvCredits;

    @BindView(R.id.coins)
    public ImageView ivCoins;

    @BindView(R.id.remaining_time)
    public TextView tvRemainingTime;

    private Context mContext;
    private League mLeague;
    private OnClickListener mlistener;
    private long mLastClickTime;

    // ---------------------------------------------------------------------------------------------
    // Copnstructors
    // ---------------------------------------------------------------------------------------------

    public AllLeaguesViewholder(View view, OnClickListener listener) {
        super(view);
        mContext = itemView.getContext();
        ButterKnife.bind(this, view);
        mlistener = listener;
    }

    // ---------------------------------------------------------------------------------------------
    // OnClick Methods
    // ---------------------------------------------------------------------------------------------

    @OnClick({R.id.join})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.join:
                if (mlistener != null) {
                    mlistener.onJoin(mLeague);
                }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Methods
    // ---------------------------------------------------------------------------------------------

    public void setItem(League league) {
        mLeague = league;

        int random = getRandomNumber(0, 3);

        if (random < 2) {
            ivCoins.setVisibility(View.VISIBLE);
            tvCredits.setText("" + getRandomNumber(3, 99));
        }

        setRemainingTime(getRandomNumber(1, 10));
        tvParticipants.setText("" + getRandomNumber(5, 35));
        tvCalendar.setText("" + getRandomNumber(7, 23));
    }

    public boolean isClickable() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return false;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    // Methods
    // ---------------------------------------------------------------------------------------------

    public void setRemainingTime(int time) {
        int random = getRandomNumber(0,1);
        if (random == 0) {
            tvRemainingTime.setText(mContext.getResources().getString(R.string.all_league_hours,time));
            tvRemainingTime.setTextColor(ContextCompat.getColor(mContext, R.color.redText));
        } else {
            tvRemainingTime.setText(mContext.getResources().getString(R.string.all_league_hours,time));
            tvRemainingTime.setTextColor(ContextCompat.getColor(mContext, R.color.greenText));
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Listeners
    // ---------------------------------------------------------------------------------------------

    public interface OnClickListener {
        void onJoin(League league);
    }

    // ---------------------------------------------------------------------------------------------
    // TEST
    // ---------------------------------------------------------------------------------------------
    private int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
