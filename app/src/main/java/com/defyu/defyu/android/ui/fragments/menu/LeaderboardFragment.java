package com.defyu.defyu.android.ui.fragments.menu;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defyu.defyu.android.R;
import com.defyu.defyu.android.data.models.Leaderboard;
import com.defyu.defyu.android.interfaces.MainActivityInterface;
import com.defyu.defyu.android.ui.adapters.LeaderboardAdapter;
import com.defyu.defyu.android.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class LeaderboardFragment extends BaseFragment{
    private static final String TAG = LeaderboardFragment.class.getSimpleName();

    private FragmentActivity context;
    private MainActivityInterface mListener;
    private LeaderboardAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Leaderboard> leaderboardList;

    @BindView(R.id.photo_player_1)
    ImageView photo_player_1;
    @BindView(R.id.photo_player_2)
    ImageView photo_player_2;
    @BindView(R.id.photo_player_3)
    ImageView photo_player_3;
    @BindView(R.id.name_player_1)
    TextView name_player_1;
    @BindView(R.id.name_player_2)
    TextView name_player_2;
    @BindView(R.id.name_player_3)
    TextView name_player_3;
    @BindView(R.id.points_player_1)
    TextView points_player_1;
    @BindView(R.id.points_player_2)
    TextView points_player_2;
    @BindView(R.id.points_player_3)
    TextView points_player_3;
    @BindView(R.id.list_position)
    RecyclerView list_position;
    @BindView(R.id.scContent)
    ScrollView scContent;

    public LeaderboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LeaderboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LeaderboardFragment newInstance() {
        return new LeaderboardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHaveToolbar(true);
        setHasOptionsMenu(true);
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_leaderboard;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.leaderboard);
        setEnableBackToolbar(true);

        recyclerLeaderboard();

    }



    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mListener = (MainActivityInterface) context;
        } catch (ClassCastException e) {
            Log.e(TAG, context.toString() + "must implement MainActivityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_leaderboard, menu);
        menu.removeItem(R.id.menu_notification);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_notification_on:
                pushFragment(new NotificationsFragment());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void recyclerLeaderboard(){

        initializeData();

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        list_position.setLayoutManager(mLayoutManager);
        mAdapter = new LeaderboardAdapter(leaderboardList);

        list_position.setAdapter(mAdapter);

        Glide.with(context).load(R.drawable.photo_testcopia_5).into(photo_player_1);
        Glide.with(context).load(R.drawable.photo_testcopia_5).into(photo_player_2);
        Glide.with(context).load(R.drawable.photo_testcopia_5).into(photo_player_3);

        name_player_1.setText("Lionel Messi");
        name_player_2.setText("Juan perez");
        name_player_3.setText("Bruno Diaz");
        points_player_1.setText(String.valueOf(98));
        points_player_2.setText(String.valueOf(89));
        points_player_3.setText(String.valueOf(87));

        list_position.setNestedScrollingEnabled(false);
    }

    private void initializeData(){
        leaderboardList = new ArrayList<>();
        leaderboardList.add(new Leaderboard(4, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        leaderboardList.add(new Leaderboard(5, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        leaderboardList.add(new Leaderboard(6, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        leaderboardList.add(new Leaderboard(7, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        leaderboardList.add(new Leaderboard(8, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        leaderboardList.add(new Leaderboard(9, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
        leaderboardList.add(new Leaderboard(10, R.drawable.photo_testcopia_5, "Jhon Doe", 86));
    }

}

